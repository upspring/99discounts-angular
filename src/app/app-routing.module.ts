import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { PostAdsComponent } from './components/post-ads/post-ads.component';
import { UploadImagesComponent } from './components/upload-images/upload-images.component';
import { CategoryComponent } from './components/category/category.component';
import { LandingPageComponent } from './components/landing-page/landing-page.component';
import { CategoryAdsComponent } from './components/category-ads/category-ads.component';
import { SingleAdComponent } from './components/single-ad/single-ad.component';
import { LocationsComponent } from './components/locations/locations.component';
import { SellerProfileComponent } from './components/seller-profile/seller-profile.component';
import{InvalidLoginComponent} from './components/invalid-login/invalid-login.component';
import{SignupComponent} from './components/signup/signup.component';
import{SigninComponent} from './components/signin/signin.component';
import{ForgetPasswordComponent} from './components/forget-password/forget-password.component'
import { MyAdsComponent } from './components/my-ads/my-ads.component';
import { AlternativeRecommentComponent } from './components/alternative-recomment/alternative-recomment.component';
import { OfferComponent } from './components/offer/offer.component';
import { UserProfileComponent } from './components/user-profile/user-profile.component';
import { ChatComponent } from './components/chat/chat.component';


const routes: Routes = [
  
  { path: 'login', component:SigninComponent},
  { path: '', component:LandingPageComponent},
  { path: 'search', component:LandingPageComponent},
  {path:'ShowCategory',component:CategoryComponent},
  {path:'PostAds/:category',component:PostAdsComponent},
  {path:'Ads/:category',component:CategoryAdsComponent},
  {path:'seller-profile/:id',component:SellerProfileComponent},
  {path: 'image', component:UploadImagesComponent },
  {path:'single-ad/:id',component:SingleAdComponent},
  {path:'locations',component:LocationsComponent},
  {path:'invalid',component:InvalidLoginComponent},
  {path:'signup',component:SignupComponent},
  
  {path:'forget',component:ForgetPasswordComponent},
  {path:'myAds/:id',component:MyAdsComponent},
  {path:'alternative-recomment/:url',component:AlternativeRecommentComponent},
  {path:'edit',component:UserProfileComponent},
  {path:'view-offer/:url',component:OfferComponent},
  {path:'chat/:itemId/:sellerId',component:ChatComponent},
  {path:'userChat',component:ChatComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
