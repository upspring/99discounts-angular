import { Component } from '@angular/core';

import * as firebase from 'firebase';

const config = {
  apiKey: 'AIzaSyC5leKbLtqO6AK336C0F_5l7rDyWSZZY0Q',
  databaseURL: 'https://angularfirebasechat-76023-default-rtdb.asia-southeast1.firebasedatabase.app/'
};

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'Angular8ClientCrud';
  constructor() {
    firebase.initializeApp(config);
  }
}
