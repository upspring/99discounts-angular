import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { HttpClientModule } from '@angular/common/http';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { NgxGalleryModule } from '@kolkov/ngx-gallery';

import { PostAdsComponent } from './components/post-ads/post-ads.component';
import { UploadImagesComponent } from './components/upload-images/upload-images.component';
import { CategoryComponent } from './components/category/category.component';
import { LandingPageComponent } from './components/landing-page/landing-page.component';
import { HeaderComponent } from './components/header/header.component';
import { FooterComponent } from './components/footer/footer.component';
import { CategoryAdsComponent } from './components/category-ads/category-ads.component';
import { SingleAdComponent } from './components/single-ad/single-ad.component';
import { SellerProfileComponent } from './components/seller-profile/seller-profile.component';
import { OfferComponent } from './components/offer/offer.component';
import { MyAdsComponent } from './components/my-ads/my-ads.component';
import { AgmCoreModule } from '@agm/core';

import { GooglePlaceModule } from "ngx-google-places-autocomplete";
import { LocationsComponent } from './components/locations/locations.component';
import { AutocompleteLibModule } from 'angular-ng-autocomplete';

import { MatAutocompleteModule } from '@angular/material/autocomplete';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatInputModule } from '@angular/material/input';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { SocialLoginModule,SocialAuthServiceConfig } from "angularx-social-login";
import { GoogleLoginProvider,FacebookLoginProvider } from "angularx-social-login";
import { InvalidLoginComponent } from './components/invalid-login/invalid-login.component';
import { NgGoogleOneTapModule } from 'ng-google-one-tap';
import { SignupComponent } from './components/signup/signup.component';
import { SlickCarouselModule } from 'ngx-slick-carousel';
import { SigninComponent } from './components/signin/signin.component';
import { SliderComponent } from './components/slider/slider.component';
import { ShareButtonsModule } from 'ngx-sharebuttons/buttons';
import { ShareIconsModule } from 'ngx-sharebuttons/icons';
import { NgxStarsModule } from 'ngx-stars';
import { ProductSliderComponent } from './components/product-slider/product-slider.component';
import { ProfileComponent } from './components/profile/profile.component';
import { ForgetPasswordComponent } from './components/forget-password/forget-password.component';
import { CodeInputModule } from 'angular-code-input';
import { AlternativeRecommentComponent } from './components/alternative-recomment/alternative-recomment.component';
import { UserProfileComponent } from './components/user-profile/user-profile.component';
import { ChatComponent } from './components/chat/chat.component';


import { MatIconModule } from '@angular/material/icon';
 
import { MatCardModule } from '@angular/material/card';
import { MatTableModule } from '@angular/material/table';
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';
import { MatSortModule } from '@angular/material/sort';
import { MatSnackBarModule } from '@angular/material/snack-bar';
import { MatSidenavModule } from '@angular/material/sidenav';
import { DatePipe } from '@angular/common'

const CLIENT_ID ="725860114805-v6pjeo9p7bmnfkanc2r5i3p08cnormvj.apps.googleusercontent.com";
@NgModule({
  declarations: [
    AppComponent,
  
  PostAdsComponent,
  UploadImagesComponent,
  CategoryComponent,
  LandingPageComponent,
  HeaderComponent,
  FooterComponent,
  CategoryAdsComponent,
  SingleAdComponent,
  LocationsComponent,
  MyAdsComponent,
  InvalidLoginComponent,
  SignupComponent,
  SigninComponent,
  OfferComponent,
  ProductSliderComponent,
  ProfileComponent,
  SliderComponent,
  SellerProfileComponent,
  ForgetPasswordComponent,
  AlternativeRecommentComponent,
  UserProfileComponent,
  ChatComponent
 

  
 
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    HttpClientModule,
   GooglePlaceModule,
   AutocompleteLibModule,
   MatAutocompleteModule,
    MatFormFieldModule,
    MatInputModule,
    BrowserAnimationsModule,
    ReactiveFormsModule,
    FormsModule,
    SocialLoginModule,
    SlickCarouselModule,
    ShareButtonsModule,
    ShareIconsModule,
    NgxGalleryModule,
    NgxStarsModule,
    MatIconModule,
    MatCardModule,
     
    MatTableModule,
    MatProgressSpinnerModule,
    MatSortModule,
    MatSnackBarModule,
    MatSidenavModule,
    NgGoogleOneTapModule.config(
    {  //Look options table for some more avaialbe options and config here.
        client_id: '725860114805-v6pjeo9p7bmnfkanc2r5i3p08cnormvj.apps.googleusercontent.com',
        cancel_on_tap_outside: false,
        authvalidate_by_googleapis: false,
        auto_select: false,
        disable_exponential_cooldowntime: false,
        context: 'signup',
        
    }),
    CodeInputModule.forRoot({
      codeLength: 6,
      isCharsCode: true,
      code: ''
    }),

  ],
  providers: [
    {
      provide: "SocialAuthServiceConfig",
      useValue: {
        autoLogin: true,
        providers: [
          {
            id: GoogleLoginProvider.PROVIDER_ID,
            provider: new GoogleLoginProvider(
              CLIENT_ID
            )
          },
          {
            id: FacebookLoginProvider.PROVIDER_ID,
            provider: new FacebookLoginProvider('861992904494130'),
          },

        ]
      } as SocialAuthServiceConfig
    },
    DatePipe
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
