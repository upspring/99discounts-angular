import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AlternativeRecommentComponent } from './alternative-recomment.component';

describe('AlternativeRecommandComponent', () => {
  let component: AlternativeRecommentComponent;
  let fixture: ComponentFixture<AlternativeRecommentComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AlternativeRecommentComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AlternativeRecommentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
