import { Component, OnInit } from '@angular/core';
import { PostAdsService } from 'src/app/services/post-ads.service';
import { ActivatedRoute, Router } from '@angular/router';
@Component({
  selector: 'app-alternative-recommand',
  templateUrl: './alternative-recomment.component.html',
  styleUrls: ['../../../assets/css/vendor/slick.min.css','../../../assets/css/vendor/bootstrap.min.css','../../../assets/css/custom/main-1.css','../../../assets/css/custom/index-1.css','../../../assets/css/custom/modal.css','../../../assets/css/custom/line-icon.css','../../../assets/css/custom/flat-icon.css']
})
export class AlternativeRecommentComponent implements OnInit {
recommentation:any;
title:any;
image_url:any;
  constructor(private postAdsServices:PostAdsService,private router: Router,
    private route: ActivatedRoute) { }

  ngOnInit(): void {
    this.route.params.subscribe(params => {
      console.log("params-->",params);
      var url = params['url'];
     
      this.getAlternativeRecomment(url);
    
     // 
  });
  }
  getAlternativeRecomment(url)
  {
    this.postAdsServices.getAlternativeRecomment(url)
    .subscribe(
      response2 => {
        console.log(response2);
        this.recommentation=response2;
        console.log("this"+this.recommentation[0].title);
        this.title=this.recommentation[0].title;
        this.image_url=this.recommentation[0].image_url1;
        for(var i=0;i<this.recommentation.length;i++)
        {
         
          console.log(this.recommentation[i].getList[0].name);
          if(this.recommentation[i].getList[0].photoUrl=="")
          {
            this.recommentation[i].getList[0].photoUrl=this.recommentation[i].getList[0].name.charAt(0);
            this.recommentation[i].getList[0].initial=true;
          }
          else{
            this.recommentation[i].getList[0].initial=false;
          }
          
        }
        console.log("--->"+this.recommentation[0].getList[0].photoUrl);
        console.log("--->"+this.recommentation[0].getList[0].initial);
        
          },
          error => {
            console.log(error);
          });
  }

}
