import { Component, OnInit,Inject } from '@angular/core';
import { PostAdsService } from 'src/app/services/post-ads.service';
import { ActivatedRoute, Router } from '@angular/router';
import { DOCUMENT } from '@angular/common'; 

@Component({
  selector: 'app-category-ads',
  templateUrl: './category-ads.component.html',
  styleUrls: ['../../../assets/css/vendor/slick.min.css','../../../assets/css/vendor/bootstrap.min.css','../../../assets/css/custom/main-1.css','../../../assets/css/custom/index-1.css','../../../assets/css/custom/modal.css','../../../assets/css/custom/line-icon.css','../../../assets/css/custom/flat-icon.css']

})
export class CategoryAdsComponent implements OnInit {

  data:any;
  ads:any;
  ad:any;
  sub_category:any;
  main_category:any;
  sub=false;
  
  constructor(private postAdsServices:PostAdsService,  private router: Router,
    private route: ActivatedRoute, @Inject(DOCUMENT) document) { 
      this.route.params.subscribe(params => {
        console.log("params-->",params);
        var id = params['category'];
        this.main_category=id;
        
       
      console.log('Url Id: ',id);
        this.getAllCategory();
        this.getAdsForCategory(id);
      
       // 
    });
    }

  ngOnInit() {
    
  //this.getAdsForCategory(this.defalut);
  //slider();
  console.log("itc calling")
  }
  ngAfterViewInit()
  {
    console.log("after view",document.getElementById("slider"));
    var titleElement = document.getElementById("myelement");
    const divs = document.querySelectorAll('div #slider');
    console.log("--->"+divs.length)
  }
  getAllCategory()
  {
    
    this.postAdsServices.getAllCategory()
    .subscribe(
      response => {
        console.log(response);
        this.data=response;
        console.log("this"+this.data.length);
        
      },
      error => {
        console.log(error);
      });
    
     
     
    
  }
  getAdsForCategory(id)
  {
    this.postAdsServices.getAdsForCategory(id)
    .subscribe(
      response2 => {
        console.log(response2);
        this.ads=response2;
        console.log("this"+this.data.length);
        
        
          },
          error => {
            console.log(error);
          });
      
    
      
  }
  getMainCategory()
  {
    this.postAdsServices.getMainCategory()
    .subscribe(
      response => {
        console.log(response);
        this.data=response;
       
        console.log("sucessfully inserted")
        
      },
      error => {
        console.log(error);
      });
  }

}
