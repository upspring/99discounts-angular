import { Component, OnInit,Input, Output, EventEmitter } from '@angular/core';
import { Router, NavigationEnd, ActivatedRoute } from '@angular/router';
import {SocialAuthService} from "angularx-social-login";

import { PostAdsService } from 'src/app/services/post-ads.service';

@Component({
  selector: 'app-category',
  templateUrl: './category.component.html',
  styleUrls: ['../../../assets/css/custom/category/custom.css','../../../assets/css/custom/category/bootstrap.css', '../../../assets/css/custom/style-category.css','../../../assets/css/custom/main-1.css','../../../assets/css/custom/index-1.css']

})
export class CategoryComponent implements OnInit {

  data:any;
  selectedIndex: number;
  categoryList2:any;
  socialUser;
   
  categoryList:any;
   
  test: string;
   loggedIn=false;
   
  constructor(private postAdsServices:PostAdsService,  private router: Router,
    private route: ActivatedRoute,private authService: SocialAuthService)  { }
       
  
  ngOnInit() {
    console.log("inside haha");


    if (localStorage.getItem("userObject") === null) {
      console.log("empty");
      window.location.href="https://99discounts.in/"; 
    }
    else{
      console.log("not empty");
       this.socialUser=JSON.parse(localStorage.getItem('userObject'));
        console.log(this.socialUser);
        console.log(this.socialUser[0].email);
         
        
        
         this.loggedIn =true; 
    }
    this.getMainCategory();
    this.getAllCategory2();
    this.selectedIndex = 0;
     //this.show(this.selectedIndex)
   
  }


 enableView()
  {
    console.log("after view init");
     document.getElementById("0").style.display="block";
  }
  activateClass(data){
   data.active = !data.active;    
  }
  show(index: number)
  {

    
     /*var i=1;
     while(i<=6)
     {console.log(i);
        if(i!=id)
        {          
          document.getElementById(i.toString()).style.display="none";                
        }
        i++;
     }
     document.getElementById(id).style.display="block";*/
     this.selectedIndex = index;
     var i=0;
     while(i<=4)
     {console.log(i);
        if(i!=index)
        {          
          document.getElementById(i.toString()).style.display="none";                
        }
        i++;
     }
     document.getElementById(index.toString()).style.display="block";
  }
  getMainCategory()
  {
    this.postAdsServices.getMainCategory()
    .subscribe(
      response => {
        console.log(response);
        this.data=response;
       
        console.log("sucessfully inserted");
        console.log("load data--->"+this.data);
        
      },
      error => {
        console.log(error);
      });
  }

  getAllCategory2()
  {
    this.postAdsServices.getAllCategory2()
    .subscribe(
      response => {
        console.log(response);
        this.categoryList2=response;
       
        console.log("sucessfully inserted"+this.categoryList2);
        
      },
      error => {
        console.log(error);
      });
  }
  
}
