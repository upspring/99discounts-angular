import { Component, OnInit, ElementRef, ViewChild } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { FormControl, FormGroupDirective, FormBuilder, FormGroup, NgForm, Validators } from '@angular/forms';
import { ErrorStateMatcher } from '@angular/material/core';
import * as firebase from 'firebase';
import { DatePipe } from '@angular/common';
import { PostAdsService } from 'src/app/services/post-ads.service';

export class MyErrorStateMatcher implements ErrorStateMatcher {
  isErrorState(control: FormControl | null, form: FormGroupDirective | NgForm | null): boolean {
    const isSubmitted = form && form.submitted;
    return !!(control && control.invalid && (control.dirty || control.touched || isSubmitted));
  }
}

export const snapshotToArray = (snapshot: any) => {
  const returnArr = [];

  snapshot.forEach((childSnapshot: any) => {
      const item = childSnapshot.val();
      item.key = childSnapshot.key;
      returnArr.push(item);
  });

  return returnArr;
};

@Component({
  selector: 'app-chat',
  templateUrl: './chat.component.html',
  styleUrls: ['./chat.component.css']
})
export class ChatComponent implements OnInit {

  @ViewChild('chatcontent') chatcontent: ElementRef;
  scrolltop: number = null;

  chatForm: FormGroup;
  nickname = '';
  roomname = '';
  message = '';
  users = [];
  chats = [];
  matcher = new MyErrorStateMatcher();
  rooms = [];
  adsData:any;
  properRooms=[];
  isLoadingResults = true;
  ref = firebase.database().ref('rooms/');
   login :any;
   room:any;
newRooms=[];
    seller:any;
   roomname1 = '';
   roomname2 = '';
    sellerId:any;
   properChats=[];
    loginUserDetails:any;
    clicked:number=0;
 roomExists:number=0;
    constructor(private router: Router,
      private route: ActivatedRoute,
      private formBuilder: FormBuilder,private postAdsServices:PostAdsService,
      public datepipe: DatePipe) {
       

        /*this.authService.authState.subscribe(user => {
          //console.log("user-->"+user);
          });*/
      
        if (localStorage.getItem('userObject') !== null) {
          //console.log(`userObject exists`);
           var myObjStr=JSON.parse(localStorage.getItem('userObject'));
            //console.log(myObjStr);
            this.loginUserDetails=myObjStr[0];
            this.nickname=this.loginUserDetails.name
          if(myObjStr==null)
          {
            //console.log(`is null`);
          //  this.loggedIn = false;
          }
          else{
         
           //this.loggedIn = true;
          } 
      
      } else {
          //console.log(`userObject not found`);
          //this.loggedIn = false;
      }

      //console.log(this.router.url);
      if(this.router.url!="/userChat")
      {
      this.route.params.subscribe(params => {
        //console.log("params-->",params);
        var id = params.itemId;
        //console.log(id);
        this.sellerId=params.sellerId;

        var title=localStorage.getItem(params.itemId);
        var sellerName=localStorage.getItem(this.sellerId);
        var date = this.datepipe.transform(new Date(), 'dd/MM/yyyy HH:mm:ss');

      this.room={'roomname1':params.sellerId+'/'+params.itemId,'roomname2':this.loginUserDetails._id+'/'+params.itemId,"sellerId":this.sellerId,"sellerName":sellerName,'buyerId':this.loginUserDetails._id,'buyerName':this.loginUserDetails.name,'productId':id,'productTitle':title,"date":date,"chated":false};
        //console.log(this.room);
      });    
                 

                var user={'nickname':this.loginUserDetails.name}
                //console.log(user.nickname);
                  this.nickname=user.nickname;

                  firebase.database().ref('users/').orderByChild('nickname').equalTo(user.nickname).once('value', snapshot => {
                  if (snapshot.exists()) {
                     
                     
                  } else {
                    const newUser = firebase.database().ref('users/').push();
                    newUser.set(user);
                     
                  }
                });
                //console.log(this.loginUserDetails._id);
                //console.log(this.sellerId);
                if(this.loginUserDetails._id !=this.sellerId)
                {
                  //console.log("ini");
                this.ref.once('value', snapshot => {
                  console.log(1);
                  var t=0;
                  
                  if (snapshot.exists()) {
                    
                    //localStorage.setItem('nickname', this.nickname);
                    this.newRooms = [];
                    this.newRooms = snapshotToArray(snapshot);
                     //console.log("resp checj--->",snapshot.val());
                      
                      
                     this.newRooms.forEach(element => {
                     if ((element.roomname1 == this.room.roomname1 && element.buyerId==this.room.buyerId) || (element.roomname2 == this.room.roomname2 && element.buyerId==this.room.buyerId)
                      
                      ) {
                        //console.log(element)
                          t=1
                          this.roomExists=1;
                     
                       }
                     });
                     firebase.database().ref('rooms/').once('value', resp => {
                       console.log(2);
                      this.rooms = [];
                      this.rooms = snapshotToArray(resp);
                       //console.log("resp--->",resp.val());
                       var item = resp.val();
                       this.properRooms=[];
                     this.rooms.forEach(element => {
                      if (element.buyerId ==this.loginUserDetails._id  || element.sellerId == this.loginUserDetails._id
                      ) {
                        //console.log("prpoper rooms",element)
                        this.properRooms.push(element);
                    // TODO: do something with the item
                  }
    
                  else{
    
                  }
                     });
                     if(this.router.url!="/userChat" && this.roomExists==0)
                     {
                       //console.log("check room")
                      this.properRooms.push(this.room);
                     }
                      this.isLoadingResults = false;
                    });


                  } 
                  else{
                    if(this.router.url!="/userChat" && this.roomExists==0)
                    {
                      //console.log("check room")
                     this.properRooms.push(this.room);
                    }
                  }
                  /*if(t==0) {
                    const newUser = firebase.database().ref('rooms/').push();
                    //console.log("new user",newUser)
                    newUser.set(this.room);
                    //localStorage.setItem('nickname', this.nickname);
                     
                  }*/
                });
              }
            }
           
            if(this.router.url=="/userChat")
      {
                firebase.database().ref('rooms/').on('value', resp => {
                  this.rooms = [];
                  this.rooms = snapshotToArray(resp);
                   //console.log("resp--->",resp.val());
                   var item = resp.val();
                   this.properRooms=[];
                 this.rooms.forEach(element => {
                  if (element.buyerId ==this.loginUserDetails._id  || element.sellerId == this.loginUserDetails._id
                  ) {
                    //console.log("prpoper rooms",element)
                    this.properRooms.push(element);
                // TODO: do something with the item
              }

              else{

              }
                 });
                  
                  this.isLoadingResults = false;
                });
              }       
            
                /*firebase.database().ref('chats/').orderByChild('roomname').equalTo('room1').on('value', resp => {
                  this.chats = [];
                  this.chats = snapshotToArray(resp);
                  setTimeout(() => this.scrolltop = this.chatcontent.nativeElement.scrollHeight, 500);
                });*/
            
              }

  ngOnInit(): void {

    this.chatForm = this.formBuilder.group({
      'message' : [null, Validators.required]
    });
  }


    onFormSubmit(form: any) {

      if (localStorage.getItem('userObject') !== null) {
        //console.log(`userObject exists`);
         var myObjStr=JSON.parse(localStorage.getItem('userObject'));
          //console.log(myObjStr);
          this.loginUserDetails=myObjStr[0];
          this.nickname=this.loginUserDetails.name;
          localStorage.setItem("nickname",this.nickname);
        if(myObjStr==null)
        {
          //console.log(`is null`);
        //  this.loggedIn = false;
        }
        else{
       
         //this.loggedIn = true;
        } 
    
    } else {
        //console.log(`userObject not found`);
        //this.loggedIn = false;
    }
    if(this.router.url!="/userChat")
    {
    if(this.roomExists==0)
    {
      const newUser = firebase.database().ref('rooms/').push();
                    //console.log("new user",newUser)
                     
                    newUser.set(this.room);
                     
    }
  }
      //console.log("chat--->",form)
      const chat = form;
      chat.roomname1 =this.roomname1;
      chat.roomname2=this.roomname2
      chat.nickname = this.nickname;
      chat.date = this.datepipe.transform(new Date(), 'dd/MM/yyyy HH:mm:ss');
      chat.type = 'message';
      const newMessage = firebase.database().ref('chats/').push();
      newMessage.set(chat);
      this.chatForm = this.formBuilder.group({
        'message' : [null, Validators.required]
      });
  }

  exitChat() {
    const chat = { roomname: '', nickname: '', message: '', date: '', type: '' };
    chat.roomname = this.roomname;
    chat.nickname = this.nickname;
    chat.date = this.datepipe.transform(new Date(), 'dd/MM/yyyy HH:mm:ss');
    chat.message = `${this.nickname} leave the room`;
    chat.type = 'exit';
    const newMessage = firebase.database().ref('chats/').push();
    newMessage.set(chat);

    firebase.database().ref('roomusers/').orderByChild('roomname').equalTo(this.roomname).on('value', (resp: any) => {
      let roomuser = [];
      roomuser = snapshotToArray(resp);
      const user = roomuser.find(x => x.nickname === this.nickname);
      if (user !== undefined) {
        const userRef = firebase.database().ref('roomusers/' + user.key);
        userRef.update({status: 'offline'});
      }
    });

  //  this.router.navigate(['/roomlist']);
  }







  enterChatRoom(roomname1: string,roomname2: string) {
     
    this.clicked=1
this.roomname1=roomname1;
this.roomname2=roomname2;
    firebase.database().ref('chats/').on('value', resp => {
      
      this.properChats = [];
      this.properChats = snapshotToArray(resp);
      //console.log(this.properChats);
      this.chats=[];
      this.properChats.forEach(element => {
        //console.log("element--->",element)
      if (element.roomname1 == this.roomname1 && element.roomname2 == this.roomname2
       ) { 
             this.chats.push(element);
        }
      });
      //console.log("chats--->",this.chats);
      setTimeout(() => this.scrolltop = this.chatcontent.nativeElement.scrollHeight, 500);
    });

 
  }


}
