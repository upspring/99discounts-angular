import { Component, OnInit } from '@angular/core';
import { PostAdsService } from 'src/app/services/post-ads.service';
import { ActivatedRoute, Router } from '@angular/router';
@Component({
  selector: 'app-forget-password',
  templateUrl: './forget-password.component.html',
  styleUrls: ['../../../assets/css/vendor/bootstrap.min.css','../../../assets/css/custom/main-1.css','../../../assets/css/custom/index-1.css','../../../assets/css/custom/modal.css','../../../assets/css/custom/line-icon.css','../../../assets/css/custom/flat-icon.css','../../../assets/css/custom/login.css' ]

})
export class ForgetPasswordComponent implements OnInit {

code:any;
  constructor(private postAdsServices:PostAdsService,private router: Router) { }

  ngOnInit(): void {
  }
   onCodeChanged(code: string) {
     console.log("oncode changed"+code)
  }
  fieldTextType: boolean;
  toggleFieldTextType() {
  this.fieldTextType = !this.fieldTextType;
  }

  // this called only if user entered full code
  onCodeCompleted(code: string) {
    console.log("complete code"+code);
    if(code==this.code)
    {
     const e2: HTMLElement = document.getElementById("pass-data");
     e2.style.display="block"; 
     const user: HTMLElement = document.getElementById("user-data");
     user.style.display="none";
     const input: HTMLElement = document.getElementById("input-code");
     input.style.display="none";
    }
    else{
     alert("Incorrect Verification Code");

    }
   
  }
  next(email:HTMLInputElement)
  {
    
      const e2: HTMLElement = document.getElementById("user-data");
     e2.style.display="block";
     alert(email.value);
     this.postAdsServices.checkEmail(email.value)
           .subscribe(
             data => {
               console.log("user data-->"+data);
                        if(Object.values(data)[0].type=="others")
                          {
                            console.log("other login")
                           // document.getElementById("email-text").style="display:block;color:red !important";
                          //  document.getElementById("email-text").innerHTML =Object.values(data)[0].statusText ;
                          const e: HTMLElement = document.getElementById("email-text");
                            e.style.color = 'red';  
                            e.style.display="block";
                            e.innerHTML="Incorrect Account";  
                          }
                          else{
                             console.log("connected");
                             
                             const e: HTMLElement = document.getElementById("input-code");
                             e.style.display="block";
                                const e2: HTMLElement = document.getElementById("user-data");
                              e2.style.display="none";
                             this.postAdsServices.sendVerificationEmail(email.value)
                              .subscribe(
                                data => {
                                  console.log("data"+Object.values(data)[0].code);
                                  this.code=Object.values(data)[0].code;
                                })
                            
                          }
             },
              error => {
                            console.log(error);
              })

  }
  changePassword(email:HTMLInputElement,newPassword:HTMLInputElement,reNewPassword:HTMLInputElement):void{
    if(newPassword.value==reNewPassword.value){
      alert('Password has been reset successfully !!!');
      var password=window.btoa(newPassword.value);
         this.postAdsServices.updatePassword(email.value,password)
           .subscribe(
             data => {
               console.log("--->"+data);
              
               if(Object.values(data)[0].photoUrl==""){
                      Object.values(data)[0].photoUrl=this.getInitials(Object.values(data)[0].name);
                                
                }
                console.log("Data"+JSON.stringify(data));
                console.log("password"+window.atob(Object.values(data)[0].password));
                Object.values(data)[0].password=password;
                console.log("Data"+JSON.stringify(data));
                localStorage.setItem('userObject', JSON.stringify(data));
                localStorage.setItem("RememberMe",JSON.stringify(data));
                this.router.navigate(["/"]);

               
           })
      //this.wrongPassword=true;
    }
    else{
      alert('Input fileds mismatched');
    }
  }
  getInitials (string) {
    var names = string.split(' '),
        initials = names[0].substring(0, 1).toUpperCase();
    
    if (names.length > 1) {
        initials += names[names.length - 1].substring(0, 1).toUpperCase();
    }
    return initials;
};


}
