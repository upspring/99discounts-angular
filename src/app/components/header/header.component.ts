import { Component, OnInit } from '@angular/core';
import { PostAdsService } from 'src/app/services/post-ads.service';
import { ActivatedRoute, Router, NavigationExtras } from '@angular/router';
import {Location} from '@angular/common';
import { FormBuilder, FormControl, FormGroup } from '@angular/forms';
import {SocialAuthService} from "angularx-social-login";
import { SocialUser } from "angularx-social-login";
import * as $ from 'jquery';
@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['../../../assets/css/custom/main-1.css']
})
export class HeaderComponent implements OnInit {

  results: any[] = [];
	searchResults: any[] = [];
  categoryList:any;
  filteredOptions;

   loggedIn=false;
  formGroup : FormGroup;
  options :any;
	constructor(private postAdsServices: PostAdsService,private router: Router,
    private route: ActivatedRoute,private location: Location,private fb : FormBuilder,private authService: SocialAuthService) { }
	
	ngOnInit() {
    
   
      this.authService.authState.subscribe(user => {
            console.log("user-->"+user);
          
          
          
          });
          if (localStorage.getItem('userObject') !== null) {
            console.log(`userObject exists`);
             var myObjStr=JSON.parse(localStorage.getItem('userObject'));
              console.log(myObjStr);
            if(myObjStr==null)
            {
              console.log(`is null`);
              this.loggedIn = false;
            }
            else{
           
             this.loggedIn = true;
            } 

        } else {
            console.log(`userObject not found`);
            this.loggedIn = false;
        } 
   
    var text="Oppo f7";
    var word = "Someword";
    console.log( word[0] === word[0].toUpperCase() ); 
    console.log("--->"+text.charAt(0).toUpperCase() + text.slice(1));
		//this.getSearchResults();
   
    this.getNames();
    
    this.getAllCategory();
	}


  searchOnKeyUp(event) {
		let input = event.target.value;
		//console.log('event.target.value: ' + input);
		console.log('this.searchResults: ' + this.searchResults);
   
    this.filterData(input);
    
	}  
  filterData(enteredData){
    this.filteredOptions = this.options.filter(item => {
      console.log("item brandName-->"+item);
      console.log("item"+enteredData);
     
      return item.toLowerCase().indexOf(enteredData.toLowerCase()) > -1
      
    })
  }
  getNames(){
    this.postAdsServices.getBrandName().subscribe(response => {
      console.log("res"+response);
      this.options = response;
      this.filteredOptions = response;
    })
  }
  getAllCategory()
  {
    this.postAdsServices.getAllCategory()
    .subscribe(
      response => {
        console.log(response);
        this.categoryList=response;
       
        console.log("sucessfully inserted")
        
      },
      error => {
        console.log(error);
      });
  }
	
	getSearchResults(): void {
		//this.postAdsServices.getBrandName().subscribe(sr => {Object.assign(this.searchResults, sr);});
    this.postAdsServices.getBrandName()
    .subscribe(
      response => {
       console.log(response);
       
        console.log("sucessfully inserted")
        Object.assign(this.searchResults, response);
      },
      error => {
        console.log(error);
      });
	}
  
  searchFromArray(arr, regex) {
		let matches = [], i;
		for (i = 0; i < arr.length; i++) {
      var value=regex.charAt(0).toLowerCase() + regex.slice(1);
      console.log("input"+value);
       if (arr[i].match(value)) {
         matches.push(arr[i]);
       }
     }
      
    //console.log('matches: ' + matches);
		return matches;
	};
  searchTerm(word)
  {
  console.log("world"+word.searchTerm);
  console.log("world"+word.location);
  console.log("world"+word.category);
 
  
  this.router.navigate(["/search"],{queryParams:{term:word.searchTerm,location:word.location,category:word.category},queryParamsHandling: 'merge', });
  //window.location.href="http://localhost:8081/#/search?term="+word.searchTerm+"&location="+word.location+"&category="+word.category;

 // this.router.navigate(["/"]);
 //this.location.go( "http://localhost:8081/#/search?term="+word.searchTerm+"&location="+word.location+"&category="+word.category );

}

goto(event)
{
//  alert("submitting"+event.target.value);
  console.log("inside of key enter"+event.target.value);
  var searchTerm=event.target.value;
 this.router.navigate(["/search"],{queryParams:{term:searchTerm,location:'',category:''},queryParamsHandling: 'merge', });
 
}
closeSearch(){

  document.getElementById('search-option').style.display="none";
}



}

