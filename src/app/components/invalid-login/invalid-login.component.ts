import { Component, OnInit } from '@angular/core';
import { CookieService } from 'ngx-cookie-service';
@Component({
  selector: 'app-invalid-login',
  templateUrl: './invalid-login.component.html',
  styleUrls: ['../../../assets/css/vendor/slick.min.css','../../../assets/css/vendor/bootstrap.min.css','../../../assets/css/custom/main-1.css','../../../assets/css/custom/index-1.css','../../../assets/css/custom/modal.css','../../../assets/css/custom/line-icon.css','../../../assets/css/custom/flat-icon.css']
})
export class InvalidLoginComponent implements OnInit {

  constructor(private cookieService: CookieService) { }

  ngOnInit(): void {
    localStorage.removeItem("userObject");
    this.cookieService.deleteAll();
    console.log("remove Cookie..")
  }

}
