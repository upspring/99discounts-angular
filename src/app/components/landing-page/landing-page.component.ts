import { Component, OnInit } from '@angular/core';
import { PostAdsService } from 'src/app/services/post-ads.service';

import { ActivatedRoute, Router } from '@angular/router';
import {SocialAuthService} from "angularx-social-login";


@Component({
  selector: 'app-landing-page',
  templateUrl: './landing-page.component.html',
  styleUrls: ['../../../assets/css/vendor/slick.min.css','../../../assets/css/vendor/bootstrap.min.css','../../../assets/css/custom/main-1.css','../../../assets/css/custom/index-1.css','../../../assets/css/custom/modal.css','../../../assets/css/custom/line-icon.css','../../../assets/css/custom/flat-icon.css']
})
export class LandingPageComponent implements OnInit {
 socialUser;
  data:any;
  categoryList:any;
  categoryList2:any;
  test: string;
   loggedIn=false;
 // public socialUser: SocialUser ;
  constructor(private postAdsServices:PostAdsService,private router: Router,
    private route: ActivatedRoute,private authService: SocialAuthService) { 
     
    }
      slides = [
            {img: "https://via.placeholder.com/600.png/09f/fff"},
            {img: "https://via.placeholder.com/600.png/021/fff"},
            {img: "https://via.placeholder.com/600.png/321/fff"},
            {img: "https://via.placeholder.com/600.png/422/fff"},
            {img: "https://via.placeholder.com/600.png/654/fff"}
          ];
          slideConfig = {"slidesToShow": 4, "slidesToScroll": 4, "arrows": true};
          addSlide() {
    this.slides.push({img: "http://placehold.it/350x150/777777"})
  }
  
  removeSlide() {
    this.slides.length = this.slides.length - 1;
  }
  
  slickInit(e) {
    console.log('slick initialized');
  }
  
  breakpoint(e) {
    console.log('breakpoint');
  }
  
  afterChange(e) {
    console.log('afterChange');
  }
  
  beforeChange(e) {
    console.log('beforeChange');
  }
          

  ngOnInit() 
  {
          
         
        this.authService.authState.subscribe(user => {
            console.log("user-->"+user);
         
      
            console.log(user);
          });
     
        console.log("local storage"+ localStorage.getItem('userObject') );
        if (localStorage.getItem("userObject") === null) {
          console.log("empty");
           // this.init();
        } 
        else{
           console.log("not empty");
           this.socialUser=JSON.parse(localStorage.getItem('userObject'));
            console.log(this.socialUser.name);
            
             //this.loggedIn =true; 
        }
     
   
    this.getAllCategory();
    this.getAllCategory2();
    this.route.queryParams.subscribe(routeParams => {
      console.log("activate route");
      console.log(routeParams['category']);
      console.log(routeParams["term"]);
      var search=routeParams['term']
      var loc=routeParams["location"];
      var category=routeParams["category"];
      this.searchParams(search,loc,category);
      
     });
  
  
  }
  searchParams(search,loc,category)
  {
    if(search==null &&loc==null&&category==null)
    {
     this.getAdsData();
    
    }
    else
    {
     console.log("its Search page");

     this.postAdsServices.getSearchData(search,loc,category)
     .subscribe(
       response => {
         console.log(response);
         this.data=response;
        
         console.log("sucessfully inserted")
         
       })
       
   }
 
     
  }
 

  getAdsData()

  {
    this.postAdsServices.getAdsData()
    .subscribe(
      response => {
        console.log(response);
        this.data=response;
       
        console.log("sucessfully inserted")
        
      },
      error => {
        console.log(error);
      });
  }
  getAllCategory()
  {
    this.postAdsServices.getAllCategory()
    .subscribe(
      response => {
        console.log(response);
        this.categoryList=response;
       
        console.log("sucessfully inserted")
        
      },
      error => {
        console.log(error);
      });
  }
  getAllCategory2()
  {
    this.postAdsServices.getAllCategory2()
    .subscribe(
      response => {
        console.log(response);
        this.categoryList2=response;
       
        console.log("sucessfully inserted")
        
      },
      error => {
        console.log(error);
      });
  }
  getVal() {
    const val = document.querySelector('input').value;
    console.log(val);
  }
  
}
