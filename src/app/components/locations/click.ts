import { Component, OnInit } from '@angular/core';  

  
@Component({  
  selector: 'app-todo',  
  templateUrl: './click.html',  
  styleUrls: ['./todo.component.css']  
})  
export class TodoComponent implements OnInit {  
  
  inputTodo:string='';  
  title='todo app';  
  todos=[];  
  
  constructor() { }  
  
  ngOnInit(): void {  
    
    this.todos = [  
      {  
        content:'first todo',  
        completed:false  
      },  
      {  
        content:'second todo',  
        completed:true  
      }  
    ]  
  }  
toggleDone(id:number){  
  this.todos.map((v,i) =>{  
    if(i==id) v.completed = !v.completed;  
    return v;  
  })  
}  
deleteTodo(id:number){  
  this.todos = this.todos.filter((v , i) => i !==id);  
  
  
}  
AddTodo(){  
  this.todos.push({  
    content:this.inputTodo,  
    completed:false  
  });  
  this.inputTodo="";  
}  
}  