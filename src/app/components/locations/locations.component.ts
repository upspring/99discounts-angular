import { Component, OnInit } from '@angular/core';

import { PostAdsService } from 'src/app/services/post-ads.service';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { map, startWith } from 'rxjs/operators';
import { FormBuilder, FormControl, FormGroup,Validators } from '@angular/forms';
export interface Employee {
  id: number;
  name: string;
  jobtype: string;
  email: string;
  address: string;
  imageUrl: string;
}
@Component({
  selector: 'app-locations',
  templateUrl: './click.html',
  styleUrls: ['./locations.component.css']
})
export class LocationsComponent implements OnInit {

  results: any[] = [];
	searchResults: any[] = [];
  data:any;
  filteredOptions;
  form = new FormGroup({
    name: new FormControl('', [Validators.required, Validators.minLength(3)]),
    url:new FormControl('', [Validators.required, Validators.minLength(3)]),
  });

  inputTodo:string='';  
  title='todo app';  
 // todos:[]; 
  formGroup : FormGroup;
  options :any;
	constructor(private postAdsServices: PostAdsService,private fb : FormBuilder) { }
  keyword = 'BrandName';
  todos = [  
    {  
      content:'first todo',  
      completed:false  
    },  
    {  
      content:'second todo',  
      completed:true  
    }  
  ]  
  /*data = [
    {
      id: 1,
      BrandName:"lg"
    },
    {
      id: 2,
     BrandName:"vivo"
    },
    {
      id: 3,
     BrandName:"whirpool"
    }];
*/
contact:any;
	ngOnInit() {



   var name=this.getInitials("ponvandu");
   alert(name);
                               
    this.contact = { 
      firstname:"",
      lastname:"",
      gender:"male",
      isToc:true,
      email:"",
      brandName:"",
      condition:"",
    };
    
    var text="Oppo f7";
    var word = "Someword";
console.log( word[0] === word[0].toUpperCase() );
    console.log("--->"+text.charAt(0).toUpperCase() + text.slice(1));
    this.initForm();
	//	this.getSearchResults();
  this.getNames();

	}
  initForm(){
    this.formGroup = this.fb.group({
      'employee' : ['']
    })
    this.formGroup.get('employee').valueChanges.subscribe(response => {
      console.log('data is ', response);
      this.filterData(response);
    })
  }
  filterData(enteredData){
    this.filteredOptions = this.options.filter(item => {
      console.log("item brandName-->"+item.BrandName);
      console.log("item"+enteredData);
      if(item.BrandName!=undefined)
      {
      return item.BrandName.toLowerCase().indexOf(enteredData.toLowerCase()) > -1
      }
    })
  }
  getNames(){
    this.postAdsServices.getBrandName().subscribe(response => {
      console.log("res"+response);
      this.options = response;
      this.filteredOptions = response;
    })
  }
	getSearchResults(): void {
		//this.postAdsServices.getBrandName().subscribe(sr => {Object.assign(this.searchResults, sr);});
    this.postAdsServices.getBrandName()
    .subscribe(
      response => {
       console.log(response);
       
        console.log("sucessfully inserted");
        console.log(response);
        this.data=response;
       
      console.log(this.data);
        //Object.assign(this.searchResults, response);
      },
      error => {
        console.log(error);
      });
	}
	
	searchOnKeyUp(event) {
		let input = event.target.value;
		//console.log('event.target.value: ' + input);
		console.log('this.searchResults: ' + this.searchResults);
		if (input.length > 1) {
			this.results = this.searchFromArray(this.searchResults, input);
		}
	}  
  searchOnKeyUp2(event) {
		let input = event.target.value;
		//console.log('event.target.value: ' + input);
		console.log('this.searchResults: ' + this.searchResults);
	  this.filterData(input);
	}  

	searchFromArray(arr, regex) {
		let matches = [], i;
		for (i = 0; i < arr.length; i++) {
      var value=regex.charAt(0).toLowerCase() + regex.slice(1);
      console.log("input"+value);
       if (arr[i].match(value)) {
         matches.push(arr[i]);
       }
     }
      
    console.log('matches: ' + matches);
		return matches;
	};
 goto(e)
  {
  
 console.log("goto"+e.target.value);
  
  document.getElementById("SearchResults").style.display="none";
}
selectEvent(item) {
  // here we can write code for doing something with selected item
}

onChangeSearch(val: string) {
  // here we can fetch data from remote location here
  // And reassign the 'data' which is binded to 'data' property.
}

onFocused(e){
  // here we can write our code for doing something when input is focused
}
searchTerm(word)
{
console.log("0-0000"+word.searchTerm);
}


 

toggleDone(id:number){  
this.todos.map((v,i) =>{  
if(i==id) v.completed = !v.completed;  
return v;  
})  
}  
deleteTodo(id:number){  
this.todos = this.todos.filter((v , i) => i !==id);  


}  
AddTodo(){  
this.todos.push({  
content:this.inputTodo,  
completed:false  
});  
this.inputTodo="";  
}  

submit(){
  console.log("inidse")
  if(this.form.status === 'VALID'){
    console.log(this.form.value);
  }
}
getInitials (string) {
  var names = string.split(' '),
      initials = names[0].substring(0, 1).toUpperCase();
  
  if (names.length > 1) {
      initials += names[names.length - 1].substring(0, 1).toUpperCase();
  }
  return initials;
};
}
