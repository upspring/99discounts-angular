import { Component, OnInit } from '@angular/core';
import {SocialAuthService} from "angularx-social-login";
import { ActivatedRoute, Router } from '@angular/router';
import { PostAdsService } from 'src/app/services/post-ads.service';
@Component({
  selector: 'app-my-ads',
  templateUrl: './my-ads.component.html',
  styleUrls: ['../../../assets/css/vendor/slick.min.css','../../../assets/css/vendor/bootstrap.min.css','../../../assets/css/custom/main-1.css','../../../assets/css/custom/index-1.css','../../../assets/css/custom/modal.css','../../../assets/css/custom/line-icon.css','../../../assets/css/custom/flat-icon.css']

})
export class MyAdsComponent implements OnInit {

  constructor(private postAdsServices:PostAdsService,private authService: SocialAuthService, private router: Router,private route: ActivatedRoute) { }

  socialUser:any;
  loggedIn=false;
  email:any;
  initial=false;
  userId:any;
  myAds:any;
  ngOnInit(): void {

    if (localStorage.getItem("userObject") === null) {
      console.log("empty");
      window.location.reload();   
    }
    else{
      console.log("not empty");
       this.socialUser=JSON.parse(localStorage.getItem('userObject'));
        console.log(this.socialUser);
        console.log(this.socialUser[0].email);
        this.email=this.socialUser[0].email;
        this.userId=this.socialUser[0].id;
        if(this.socialUser[0].type=="99Discounts")
        {
          this.initial=true;
        }
        
         this.loggedIn =true; 
    }
    this.route.params.subscribe(params => {
      console.log("params-->",params);
      var id = params['id'];
      console.log("id"+id);
      
      var emailId=window.atob( decodeURIComponent(id));
      console.log(emailId);
      
      this.getMyAds(emailId);
  });
    

  }

  getMyAds(email)
  {
    this.postAdsServices.getMyAds(email)
    .subscribe(
      data => {
        console.log("data-->",data);
        
        this.myAds = data;
        //this.categoryMetaData="";
        console.log(data);
      },
      error => {
        console.log(error);
      });

  }
  deletePost(event){
    var id=event.target.id;
   
    var title=document.getElementById('title_'+id).innerHTML;
   
    document.getElementById('popup-title').innerHTML=title;
    document.getElementById('delete-modal').style.display="block";
    var deleteAds=(<HTMLInputElement>document.getElementById("delete"));
    deleteAds.addEventListener('click', (evt) => this.deleteMyAds(id));
   }
  closeDelete(){
    document.getElementById('delete-modal').style.display="none";
  }
 

  deleteMyAds(id)
  {
   console.log("inside of delete Ads");
    this.postAdsServices.deleteMyAds(id)
    .subscribe(
      data => {
        console.log("data"+data);
     //   var els = document.getElementById('myads_'+id);
      //  console.log(els);
    //    els.remove();
        //window.location.reload();
        window.location.href="https://99discounts.in/";
        console.log("remove suucessfully")

      //  this
      },
      error =>{
        console.log(error);
      }
    )
  }
}
