import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import {SocialAuthService} from "angularx-social-login";
import { PostAdsService } from 'src/app/services/post-ads.service';

@Component({
  selector: 'app-offer',
  templateUrl: './offer.component.html',
  styleUrls: ['../../../assets/css/vendor/slick.min.css','../../../assets/css/vendor/bootstrap.min.css','../../../assets/css/custom/main-1.css','../../../assets/css/custom/index-1.css','../../../assets/css/custom/modal.css','../../../assets/css/custom/line-icon.css','../../../assets/css/custom/flat-icon.css']
})
export class OfferComponent implements OnInit {

  loggedIn=false;
  adsData:any;
  couponText:any;
  title:any;
  avgRating:any;
 userName:any;
  userPrice:any;
  userId:any;
  buyerEmail:any;
  phoneNumber:any;
  viewOffers:any;
  sellerPrice:any;
  sellerContent:any;
  socialUser:any;
  email:any;
  initial=false;

  constructor( private router: Router, private postAdsService:PostAdsService,
    private route: ActivatedRoute,private authService: SocialAuthService) { 
 }
   

 


  ngOnInit(): void {

//    var url="https://www.flipkart.com/hp-v22ow-64-gb-pen-drive/p/itm0fd77f1a5f90b";
    //url=decodeURIComponent(url);
    
    this.route.params.subscribe(params => {
      console.log("params-->",params);
      var id = params['url'];
console.log("id--",id)
var decode_id=window.atob(id);
console.log(decode_id)
this.getViewOffers(decode_id);

if(this.socialUser[0].photoUrl=="")
{
  this.socialUser[0].photoUrl=this.socialUser[0].name.charAt(0);
}
else{this.socialUser[0].photoUrl=this.socialUser[0].photoUrl;}
if(this.socialUser[0].type=="99Discounts")
{
  this.initial=true;
}






      /*this.authService.authState.subscribe(user => {
        console.log("user-->"+user);
      });*/
    /*
      if (localStorage.getItem('userObject') !== null) {
        console.log(`userObject exists`);
         var myObjStr=JSON.parse(localStorage.getItem('userObject'));
          console.log(myObjStr);
        if(myObjStr==null)
        {
          console.log(`is null`);
          this.loggedIn = false;
        }
        else{
       
         this.loggedIn = true;
        } 
    
        } else {
        console.log(`userObject not found`);
        this.loggedIn = false;
        }*/ 
      });
  }


  closeOffer(){
    document.getElementById('send-offer').style.display="none";
  }
  openOffer(email,amount,name){
    console.log(email)
     this.buyerEmail=email;
     this.userName=name;
     this.userPrice=amount;
    document.getElementById('send-offer').style.display="block";
  }


  send(price,content)
  {
      console.log(price);
      console.log(content);
      console.log(this.buyerEmail);
      console.log(this.title);

    this.postAdsService.sendOfferEmail(price,content,this.buyerEmail,this.title,this.userName,this.userPrice)
    .subscribe (
      data =>{
          console.log("inside send email");

      },
      error =>{
        console.log(error);
      }
    );

    document.getElementById('send-offer').style.display="none";
  }



  getViewOffers(url)
  {
    this.postAdsService.getViewOffers(url)
    .subscribe(
      data => {
          console.log("datattata",data);
         this.viewOffers=data;
         this.title=data[0].title
           
      },
      error =>{

        console.log(error);
      }
    );
  }
}
