import { Component, OnInit,Inject } from '@angular/core';
import { PostAdsService } from 'src/app/services/post-ads.service';
import { UploadFilesService } from 'src/app/services/upload-files.service';
import { ActivatedRoute, Router } from '@angular/router';
import { DOCUMENT } from '@angular/common'; 
import * as firebase from 'firebase';
import { WindowService } from 'src/app/services/window.service';
@Component({
  selector: 'app-post-ads',
  templateUrl: './post-ads.component.html',
  styleUrls: ['../../../assets/css/vendor/slick.min.css','../../../assets/css/vendor/bootstrap.min.css','../../../assets/css/custom/main-1.css','../../../assets/css/custom/index-1.css','../../../assets/css/custom/custom-style.css','../../../assets/css/custom/fancy_file_upload.css']
})
export class PostAdsComponent implements OnInit {
  selectedFiles: FileList;
  submitted = false;
  fileList: File[] = [];
  listOfFiles: any[] = [];
  fileStatus=false;
  category:any;
  containers = [];
  categoryMetaData=null;
  socialUser:any;
  email:any;
  postAds:any;
  status=false;
  windowRef: any;
  verificationCode: string;
  number:any;
  num:any;
  dataObject:any;
  isVerified=false;
  mobile_no:any;
  constructor(
    private postAdsServices:PostAdsService,
    private uploadService: UploadFilesService,
    private router: Router,
    private route: ActivatedRoute,private win: WindowService,
    @Inject(DOCUMENT) document) 
    { 
      const firebaseConfig = {
        apiKey: "AIzaSyA3OT3a5K9IT_gAlC7ybfs1zHCfc8qtC8c",
        authDomain: "discounts-bdc9b.firebaseapp.com",
        projectId: "discounts-bdc9b",
        storageBucket: "discounts-bdc9b.appspot.com",
        messagingSenderId: "167744934208",
        appId: "1:167744934208:web:61a4fff99ab995d0936aa5",
        measurementId: "G-BNKXDSKHPP"
      };
      if (!firebase.apps.length) {
        firebase.initializeApp(firebaseConfig);
      }
    }

  ngOnInit() {
   
        if (localStorage.getItem("userObject") === null) {
          console.log("Not Login..");
           
        } 
        else{
           console.log("Logged In");
           this.socialUser=JSON.parse(localStorage.getItem('userObject'));
            console.log(this.socialUser[0]);
            console.log(this.socialUser[0].phone_status);
            this.email=this.socialUser[0].email;
            if(this.socialUser[0].phone_status=="verified")
            {
              console.log("inside of verified");
              this.isVerified=true;
              this.mobile_no=this.socialUser[0].phone_number;
              console.log("phone"+this.mobile_no);
            }
            
            
        }
        this.route.params.subscribe(params => {
          console.log("params-->",params);
          var id = params['category'];
          this.category=id;
        
        console.log('Url Id: ',id);
          this.getMetadata(id);
        });
        this.windowRef = this.win.windowRef;
      
        // this.windowRef.recaptchaVerifier = new firebase.auth.RecaptchaVerifier('recaptcha-container');
       this.windowRef.recaptchaVerifier = new firebase.auth.RecaptchaVerifier(
         "recaptcha-container",
         {
           size: "invisible",
           callback: function(response) {
             //submitPhoneNumberNumber();
           }
         }
       );
         this.windowRef.recaptchaVerifier.render();
         console.log("recap"+this.windowRef.recaptchaVerifier)

  }
  getMetadata(id)
  {

    this.postAdsServices.getMetadata(id)
    .subscribe(
      data => {
        this.categoryMetaData = data[0].metadata;
        //this.categoryMetaData="";
        console.log(data);
      },
      error => {
        console.log(error);
      });
  }
  add() {
    if(this.containers.length>2)
    {
      return false;
    }
    this.containers.push(this.containers.length);
    console.log("inside of add"+this.containers.length);
    //document.getElementById("test").style.display="none";
  }
  remove(event) {
  
  //  this.containers.slice(this.containers.length);
  console.log("i value"+ event.target.id);
  let parent = ( <HTMLElement>( <HTMLElement>event.target ).parentNode );
  
    var len=this.containers.length;
    console.log("deleteing"+len);
    var els = document.getElementsByClassName('target_'+event.target.id);
    console.log(els);

    els[0].remove();
   
  
  }
  selectFiles(event) {
    console.log("selected"+event.target.files);
   
    this.selectedFiles = event.target.files;
    console.log("selected"+this.selectedFiles);
  
    this.listOfFiles.push(this.selectedFiles)
  }
  
  upload( idx,file) {
   console.log()
    this.uploadService.upload(file,idx).subscribe(event =>{
     if(this.listOfFiles.length - 1 === idx) {
      
        this.fileStatus=true;
    }
    });
    
  }

  uploadFiles() {
   
  console.log("len"+this.listOfFiles.length);
    for (let i = 0; i < this.listOfFiles.length; i++) {
      console.log(i, this.listOfFiles[i][0].name);
      this.upload(i, this.listOfFiles[i][0]);
     /* if(this.listOfFiles.length - 1 === i) {
        console.log('loop ends');*/
   
    }
    //this.fileStatus=true;

  }
  otpClose(){
    document.getElementById('otp-box').style.display="none";
  }
 
  saveAds(data) {
   
    const e: HTMLElement = document.getElementById("alert-text");
    e.style.color = 'red'; e.style.marginTop='110px';e.style.background="rgba(0,0,0,.03)";e.style.border="1px solid rgba(0,0,0,.125)"
    e.style.width="620px";e.style.padding="4px";e.style.paddingLeft="10px";e.style.marginLeft= "-360px";
    console.log("--->"+data);
    console.log("--->"+data.Condition);
    console.log("--->"+data.TargetLink);
    console.log("--->"+data.TargetLink1);
    console.log("--->"+data.TargetLink2);
    console.log("--->"+data.image_url1);

    if(data.TargetLink!=undefined)
    {
    if(data.TargetLink.indexOf('https://www.amazon.')!=-1 && data.TargetLink.indexOf("ref=")!=-1)
    {
    console.log("its amazon"+data.TargetLink)
   
    let url=data.TargetLink;
    let res=url.substr(0, url.indexOf("ref"));
    
    
    data.TargetLink=res;
    }}
    if(data.TargetLink_1!=undefined)
    {
    if((data.TargetLink_1.indexOf('https://www.amazon.')!=-1 && data.TargetLink_1.indexOf("ref=")!=-1))
    { console.log("amazon line 1");
    let url=data.TargetLink_1;
    let res=url.substr(0, url.indexOf("ref"));
     data.TargetLink_1=res;
    }
    }
    if(data.TargetLink_2!=undefined)
    {
    if(data.TargetLink_2.indexOf('https://www.amazon.')!=-1 && data.TargetLink_2.indexOf("ref=")!=-1)
    { console.log("amazon line 2");
    let url=data.TargetLink_2;
    let res=url.substr(0, url.indexOf("ref"));
     data.TargetLink_2=res;
    }}
    if(data.TargetLink_3!=undefined)
    {
    if(data.TargetLink_3.indexOf('https://www.amazon.')!=-1 && data.TargetLink_3.indexOf("ref=")!=-1)
    { console.log("amazon line 3");
    let url=data.TargetLink_3;
    let res=url.substr(0, url.indexOf("ref"));
     data.TargetLink_3=res;
    }}
   
    if(data.TargetLink!=undefined)
    {
    if(data.TargetLink.indexOf('https://www.flipkart.')!=-1 && data.TargetLink.indexOf("pid=")!=-1)
    {
      console.log("flip kart....");
   
    let url=data.TargetLink;
    let res=url.substr(0,url.indexOf("?pid="));
     data.TargetLink=res;
  
    }}
    if(data.TargetLink_1!=undefined)
    {
    if((data.TargetLink_1.indexOf('https://www.flipkart.')!=-1 && data.TargetLink_1.indexOf("pid=")!=-1)){  
      console.log("flip kart line 1");
      let url=data.TargetLink_1;
      let res=url.substr(0, url.indexOf("?pid="));
       data.TargetLink_1=res;
    }}
    if(data.TargetLink_2!=undefined)
    {
    if(data.TargetLink_2.indexOf('https://www.flipkart.')!=-1 && data.TargetLink_2.indexOf("pid=")!=-1){ 
      console.log("flip line 2");
      let url=data.TargetLink_2;
      let res=url.substr(0, url.indexOf("?pid="));
    
       data.TargetLink_2=res;
      
    }}
    if(data.TargetLink_3!=undefined)
    {
    if(data.TargetLink_3.indexOf('https://www.flipkart.')!=-1 && data.TargetLink_3.indexOf("pid=")!=-1){
     console.log("flip line 3");
     let url=data.TargetLink_3;
     let res=url.substr(0, url.indexOf("?pid="));
      data.TargetLink_3=res;
    }}
   
    
    if(data.BrandName=="") { e.style.display="block";e.innerHTML="Please Enter Brand Name *"; 
    return false;
    }
    if(data.ClothingType=="") { e.style.display="block"; e.innerHTML="Please Enter Clothing Type"; 
    return false;
    }
    if(data.Size=="") {  e.style.display="block";e.innerHTML="Please Enter Size"; 
    return false;
    }
    if(data.Modal=="") { e.style.display="block";e.innerHTML="Please Enter Modal"; 
    return false;
    }
   if(data.YearofPurchase=="") { e.style.display="block";e.innerHTML="Please Enter Year of  Purchase"; 
    return false;
    }
    if(data.PhysicalCondition=="") {  e.style.display="block";e.innerHTML="Please Enter PhysicalCondition"; 
    return false;
    }
    if(data.Loading=="") {  e.style.display="block"; e.innerHTML="Please Enter Loading"; 
    return false;
    }
    if(data.Operation=="") { e.style.display="block";e.innerHTML="Please Enter Operation"; 
    return false;
    }
    if(data.Condition=="") {  e.style.display="block";e.innerHTML="Please Enter Condition"; 
    return false;
    }
    if(data.Capacity=="") {  e.style.display="block";e.innerHTML="Please Enter Capacity"; 
    return false;
    }
    if(data.KMSdriven=="") {  e.style.display="block";e.innerHTML="Please Enter KMSdriven"; 
    return false;
    }
    if(data.YearofRegistration=="") { e.style.display="block"; e.innerHTML="Please Enter YearofRegistration"; 
    return false;
    }
    if(data.title==""){  e.style.display="block";e.innerHTML="Please Enter Ad Title"; 
    return false;
    }
    if(data.description=="") { e.style.display="block";e.innerHTML="Please Enter Description"; 
    return false;
    }
    if(data.price=="") { e.style.display="block";e.innerHTML="Please Enter Price"; 
      return false;
    }
    if(data.price_type=="") { e.style.display="block";e.innerHTML="Please Enter Price Type"; 
    return false;
    }
    if(data.ad_type=="") { e.style.display="block";e.innerHTML="Please Enter Ad Type"; 
    return false;
    }
    if(!this.fileStatus){e.style.display="block"; e.innerHTML="Please Upload Photos"; 
    return false;
    }
    if(data.location=="") {  e.style.display="block";e.innerHTML="Please Enter location"; 
    return false;
    }
    if(data.mobile_no=="") {  e.style.display="block";e.innerHTML="Please Enter Valid Mobile Number"; 
    return false;
    }
    
   
    data.category=this.category;
    var today = new Date();

    var date = today.getFullYear()+'-'+(today.getMonth()+1)+'-'+today.getDate();
    data.date_created=date;
    data.createdBy=this.email;
    console.log("email is"+this.email);
    for (let i = 0; i < this.listOfFiles.length; i++) {
      console.log(i, this.listOfFiles[i][0].name);
      if(i==0)
      {
     data.image_url1="https://lutwfileupload.s3.amazonaws.com/"+ this.listOfFiles[i][0].name;
      }
      if(i==1)
      {
     data.image_url2="https://lutwfileupload.s3.amazonaws.com/"+ this.listOfFiles[i][0].name;
      }
      if(i==2)
      {
     data.image_url3="https://lutwfileupload.s3.amazonaws.com/"+ this.listOfFiles[i][0].name;
      }
      if(i==3)
      {
     data.image_url4="https://lutwfileupload.s3.amazonaws.com/"+ this.listOfFiles[i][0].name;
      }
      
    }
     console.log("--->"+data.image_url1);
     console.log("--> is verified"+this.socialUser[0].phone_status);
     this.num = data.mobile_no;
     this.number=`+91${this.num}`;
     console.log("number"+this.num);
     if(this.socialUser[0].phone_status=="unverified")
     {
       
      this.status=true;
      this.dataObject=data;
      console.log("un verifed if condition"+this.status);
      const e: HTMLElement = document.getElementById("otp-box");
      e.style.display="block";
     // e.style.marginTop="-100px";
      this.sendLoginCode();
     }
     else{
       data.mobile_no=this.mobile_no;
      this.postAdsServices.saveAd(data)
      .subscribe(
        response => {
          console.log(response);
          this.submitted = true;
          console.log("sucessfully inserted")
          this.router.navigate(["/"]);
        },
        error => {
          console.log(error);
        });
     }
      /* this.postAdsServices.saveAd(data)
          .subscribe(
            response => {
              console.log(response);
              this.submitted = true;
              console.log("sucessfully inserted")
              this.router.navigate(["/"]);
            },
            error => {
              console.log(error);
            });*/
      }
    
sendLoginCode() {
  console.log("send otp")
        const appVerifier = this.windowRef.recaptchaVerifier;
         const num = this.number;
         console.log("num"+num);
      
         
         firebase.auth().signInWithPhoneNumber(num,appVerifier).then(result => {
           console.log("result"+result);
          this.windowRef.confirmationResult = result;
         })
         .catch( error => console.log(error) );
 }
 onCodeChanged(code: string) 
 {
   console.log(code);
   this.verificationCode=code;
 } 
 onCodeCompleted(code: string) 
 {
   console.log(code);
   this.verificationCode=code;
 }
       verifyLoginCode() {
        console.log("verification code id"+this.verificationCode);
        this.windowRef.confirmationResult.confirm(this.verificationCode).then( result => {
         // this.user = result.user;
         
         this.postAdsServices.updateUser(this.email,this.num)
          .subscribe(response2 => {
            console.log("statis"+ Object.values(response2)[0].phone_status)
            Object.values(response2)[0].phone_status="verified";
            Object.values(response2)[0].phone_number=this.num;
            console.log("statis"+ JSON.stringify(response2))
            localStorage.setItem('userObject', JSON.stringify(response2));
           
          });
         this.postAdsServices.saveAd(this.dataObject)
          .subscribe(
            response => {
              console.log(response);
              this.submitted = true;
              console.log("sucessfully inserted")
              this.router.navigate(["/"]);
            },
            error => {
              console.log(error);
            });
        })
        .catch( error => alert( "Incorrect code entered?"));
      }
}





