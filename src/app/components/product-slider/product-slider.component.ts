import { Component, OnInit } from '@angular/core';
import { PostAdsService } from 'src/app/services/post-ads.service';
declare function slider() :any;

@Component({
  selector: 'app-product-slider',
  templateUrl: './product-slider.component.html',
  styleUrls: ['../../../assets/css/vendor/slick.min.css','../../../assets/css/vendor/bootstrap.min.css','../../../assets/css/custom/main-1.css','../../../assets/css/custom/index-1.css','../../../assets/css/custom/modal.css','../../../assets/css/custom/line-icon.css','../../../assets/css/custom/flat-icon.css']
})
export class ProductSliderComponent implements OnInit {

  data:any;
  categoryList:any;
  categoryList2:any;
  constructor(private postAdsServices:PostAdsService) { }

  ngOnInit() {
    //slider();
   
  
this.getAdsData();
this.getAllCategory();
this.getAllCategory2();
  }
  public loadJsFile(url) {
    let node = document.createElement('script');
    node.src = url;
    node.type = 'text/javascript';
    document.getElementsByTagName('head')[0].appendChild(node);
  }
  public loadJsFile1(url) {
    let node = document.createElement('script');
    node.src = url;
    node.type = 'text/javascript';
    document.getElementsByTagName('head')[0].appendChild(node);
  }
  getAdsData()

  {
    this.postAdsServices.getAdsData()
    .subscribe(
      response => {
        console.log(response);
        this.data=response;
       
        console.log("sucessfully inserted")
        
      },
      error => {
        console.log(error);
      });
  }
  getAllCategory()
  {
    this.postAdsServices.getAllCategory()
    .subscribe(
      response => {
        console.log(response);
        this.categoryList=response;
       
        console.log("sucessfully inserted")
        
      },
      error => {
        console.log(error);
      });
  }
  getAllCategory2()
  {
    this.postAdsServices.getAllCategory2()
    .subscribe(
      response => {
        console.log(response);
        this.categoryList2=response;
       
        console.log("sucessfully inserted")
        
      },
      error => {
        console.log(error);
      });
  }
  getVal() {
    const val = document.querySelector('input').value;
    console.log(val);
  }
  slides = [
    {img: "https://via.placeholder.com/600.png/09f/fff"},
    {img: "https://via.placeholder.com/600.png/021/fff"},
    {img: "https://via.placeholder.com/600.png/321/fff"},
    {img: "https://via.placeholder.com/600.png/422/fff"},
    {img: "https://via.placeholder.com/600.png/654/fff"}
  ];
  slideConfig = {"slidesToShow": 4, "slidesToScroll": 4, "arrows": true};
  
  addSlide() {
    this.slides.push({img: "http://placehold.it/350x150/777777"})
  }
  removeSlide() {
    this.slides.length = this.slides.length - 1;
  }
  
  slickInit(event:any) {
    console.log('slick initialized');
  }
  
  breakpoint(event:any) {
    console.log('breakpoint');
  }
  
  afterChange(event:any) {
    console.log('afterChange');
  }
  
  beforeChange(event:any) {
    console.log('beforeChange');
  }  

}
