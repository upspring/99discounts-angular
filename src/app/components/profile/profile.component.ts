import { Component, OnInit } from '@angular/core';
import {SocialAuthService} from "angularx-social-login";
import { ActivatedRoute, Router } from '@angular/router';
import { NgOneTapService } from 'ng-google-one-tap';
import { PostAdsService } from 'src/app/services/post-ads.service';
import { CookieService } from 'ngx-cookie-service';
@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.css', '../../../assets/css/custom/main-1.css', '../../../assets/css/custom/index-1.css']
})
export class ProfileComponent implements OnInit {

  socialUser:any;
  loggedIn=false;
  email:any;
  initial=false;

  constructor(private authService: SocialAuthService,private postAdsServices:PostAdsService,
    private router: Router,private onetap: NgOneTapService,
    private route: ActivatedRoute,private cookieService: CookieService) { }

  ngOnInit() {

    if (localStorage.getItem("userObject") === null) {
      console.log("empty");
        this.init();
    } 
    else{
       console.log("not empty");
       this.socialUser=JSON.parse(localStorage.getItem('userObject'));
        console.log(this.socialUser.name);
        console.log(this.socialUser[0].email);
        console.log("-->"+this.socialUser[0].age);
        this.email=this.socialUser[0].email;
        if(this.socialUser[0].photoUrl=="")
        {
          this.socialUser[0].photoUrl=this.socialUser[0].name.charAt(0);
        }
        else{this.socialUser[0].photoUrl=this.socialUser[0].photoUrl;}
        if(this.socialUser[0].type=="99Discounts")
        {
          this.initial=true;
        }
        
         this.loggedIn =true; 
    }
  
  }
  init(): void {
    this.onetap.tapInitialize(); //Initialize OneTap, At intial time you can pass config  like this.onetap.tapInitialize(conif) here config is optional.
          this.onetap.promtMoment.subscribe(res => {  // Subscribe the Tap Moment. following response options all have self explanatory. If you want more info pls refer official document below attached link.
             res.getDismissedReason(); 
             res.getMomentType();
             res.getNotDisplayedReason();
             res.getSkippedReason();
             res.isDismissedMoment();
             res.isDisplayed();
             res.isNotDisplayed();
             res.isSkippedMoment();
            
          });
          this.onetap.oneTapCredentialResponse.subscribe(res => { // After continue with one tap JWT credentials response.
              console.log(res);
              this.postAdsServices.oneTapLogin(res)
            .subscribe(
              data => {
                console.log(data);
                
                
                    if(Object.values(data)[0].type=="others")
                    {
                    
                      console.log("connected with others"+Object.values(data)[0].type);
                      this.authService.signOut();
                      this.router.navigate(["/invalid"]);
                    }else{
                        console.log("success"+Object.values(data)[0].type);
                        const dateNow = new Date(2024, 1, 25, 13, 30, 30);
                         this.cookieService.set('userObject',JSON.stringify(data),dateNow);
                        localStorage.setItem('userObject', JSON.stringify(data));
                        window.location.reload();
                    }
                  
              })
          });
    }
    public signOut(): void {
      this.authService.signOut();
      localStorage.removeItem("userObject");
      //this.cookieService.delete("userObject");
      this.cookieService.deleteAll();
      console.log(window.location.href);
      if(window.location.href=="http://localhost:8081/" ||(window.location.href=="https://99discounts.in/") )
      {
       // alert("its landing page..");
        window.location.reload();
      }
      else
      {
      this.router.navigate(["/"]);
      }
     // window.location.reload();
     }
     myAds()
     {
       console.log("inside of ads")
       console.log("email"+this.email);
       var emailId=window.btoa(this.email);
      this.router.navigate(["/myAds",emailId]);
     }

     chats()
     {
      this.router.navigate(["/userChat"]);

     }
}
