import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import {SocialAuthService} from "angularx-social-login";
import { PostAdsService } from 'src/app/services/post-ads.service';

@Component({
  selector: 'app-seller-profile',
  templateUrl: './seller-profile.component.html',
  styleUrls: ['../../../assets/css/vendor/slick.min.css','../../../assets/css/vendor/bootstrap.min.css','../../../assets/css/custom/main-1.css','../../../assets/css/custom/index-1.css','../../../assets/css/custom/modal.css','../../../assets/css/custom/line-icon.css','../../../assets/css/custom/flat-icon.css']
})
export class SellerProfileComponent implements OnInit {

  loggedIn=false;
  adsData:any;
  couponText:any;
  price:any;
  avgRating:any;
  totalRating:any;
  totalUsersRated:any;
  userId:any;
  sellerAds:any;
  phoneNumber:any;
  sellerDetails:any;

  constructor( private router: Router, private postAdsService:PostAdsService,
    private route: ActivatedRoute,private authService: SocialAuthService) { 
 }
   


  ngOnInit(): void {
    this.route.params.subscribe(params => {
      console.log("params-->",params);
      var id = params['id'];
console.log("id--",id)
      this.getSellerInfo(id);

      /*this.authService.authState.subscribe(user => {
        console.log("user-->"+user);
      });*/
    /*
      if (localStorage.getItem('userObject') !== null) {
        console.log(`userObject exists`);
         var myObjStr=JSON.parse(localStorage.getItem('userObject'));
          console.log(myObjStr);
        if(myObjStr==null)
        {
          console.log(`is null`);
          this.loggedIn = false;
        }
        else{
       
         this.loggedIn = true;
        } 
    
        } else {
        console.log(`userObject not found`);
        this.loggedIn = false;
        }*/ 
      });
  }


getSellerInfo(id)
{

  console.log("getSeller Details");
    this.postAdsService.getSellerDetails(id)
    .subscribe(
      data => {
          console.log("datattata",data);
          this.sellerDetails=data[0];
          this.sellerAds=data[0].getList;
           
      },
      error =>{

        console.log(error);
      }
    );
}

}
