import { Component, OnInit } from '@angular/core';
import {SocialAuthService} from "angularx-social-login";
import { SocialUser } from "angularx-social-login";
import { GoogleLoginProvider,FacebookLoginProvider } from "angularx-social-login";
import { ActivatedRoute, Router } from '@angular/router';
import { PostAdsService } from 'src/app/services/post-ads.service';
import { FormGroup,  FormBuilder, Validators,AbstractControl,FormControl} from '@angular/forms';
import { CookieService } from 'ngx-cookie-service';
declare var localStorage: Storage;
@Component({
  selector: 'app-signin',
  templateUrl: './signin.component.html',
  styleUrls: ['../../../assets/css/vendor/bootstrap.min.css','../../../assets/css/custom/main-1.css','../../../assets/css/custom/index-1.css','../../../assets/css/custom/modal.css','../../../assets/css/custom/line-icon.css','../../../assets/css/custom/flat-icon.css','../../../assets/css/custom/login.css' ]

})
export class SigninComponent implements OnInit {

   socialUser ;
   public social:SocialUser;
  loggedIn=true;
  rem:any;
   initial=false;
  constructor(private authService: SocialAuthService, private router: Router, 
  private postAdsServices:PostAdsService,
  private formBuilder: FormBuilder, private cookieService: CookieService
   ) {}
  ngOnInit() {
  console.log("------>login component");

       
    this.authService.authState.subscribe(user => {
     console.log("user-->"+user);
            if(user!=null)
            {
              this.social=user;
              var name= this.social.name;
              var email=this.social.email;
              var photoUrl=this.social.photoUrl;
              var type=this.social.provider;
              console.log("name is"+name);
                  this.postAdsServices.saveUser(name,email,photoUrl,type)
                  .subscribe(
                    data => {
                      
                      console.log("user data-->"+data);
                        if(Object.values(data)[0].type=="others")
                          {
                          
                            console.log("connected with others"+Object.values(data)[0].type);
                            this.authService.signOut();
                            this.router.navigate(["/invalid"]);
                          }else{
                              console.log("success"+Object.values(data)[0].type);
                              const dateNow = new Date(2024, 1, 25, 13, 30, 30);
                              this.cookieService.set( 'userObject',JSON.stringify(data),dateNow);
                              localStorage.setItem('userObject', JSON.stringify(data));
                            
                              this.router.navigate(["/"]);
                          }
                      },
                      error => {
                            console.log(error);
                      })
            }
      })
         if (localStorage.getItem("userObject") === null) {
          console.log("empty")
        } 
        else{
           console.log("not empty");
           this.socialUser=JSON.parse(localStorage.getItem('userObject'));
            console.log(this.socialUser.name);
            
             this.loggedIn =true; 
           
        }
        this.rememberMe();
       
}
rememberMe()
{
    if (localStorage.getItem("rem")) {
      
        console.log("enabled"+localStorage.getItem('RememberMe'));
        this.rem=JSON.parse(localStorage.getItem('RememberMe'));
        console.log("this.rem"+this.rem[0].password)
        this.loginForm.get("email").setValue(this.rem[0].email);
        this.loginForm.get("password").setValue(window.atob(this.rem[0].password));
      //alert(window.atob(this.rem.password));
         this.loginForm.get("rememberMe").setValue(true);
    }
          
}

googleLoginOptions = {
  scope: 'profile email'
};
public signInWithGoogle(): void {
 
 this.authService.signIn(GoogleLoginProvider.PROVIDER_ID);
}
signInWithFB(): void {
  this.authService.signIn(FacebookLoginProvider.PROVIDER_ID);
}
public signOut(): void {
 this.authService.signOut();
}
 fieldTextType: boolean;
  toggleFieldTextType() {
  this.fieldTextType = !this.fieldTextType;
  }
loginForm= new FormGroup({
      
      email:new FormControl('',[Validators.required,Validators.email]),
      password:new FormControl('',[Validators.required,Validators.minLength(8)]),
      type:new FormControl(''),
      rememberMe:new FormControl(false)
     
    
    });

login()
{
  this.loginForm.get("type").setValue("99Discounts");
   var email=this.loginForm.value.email.trim();
    var password=this.loginForm.value.password;
    password=window.btoa(password);
   
    var remember=this.loginForm.value.rememberMe;
     var type=this.loginForm.value.type;
     console.log("email"+email+password)
      this.postAdsServices.login(email,password,type)
           .subscribe(
             data => {
               console.log("user data-->"+data);
                        if(Object.values(data)[0].type=="others")
                          {
                            console.log("other login")
                           // document.getElementById("email-text").style="display:block;color:red !important";
                          //  document.getElementById("email-text").innerHTML =Object.values(data)[0].statusText ;
                          const e: HTMLElement = document.getElementById("email-text");
                            e.style.color = 'red';  
                            e.style.display="block";
                            e.innerHTML=Object.values(data)[0].statusText;  
                          }
                          else{
                             console.log("connected");
                             
                            
                             if(Object.values(data)[0].photoUrl=="")
                             {
                               console.log("name"+Object.values(data)[0].name);
                               console.log(this.getInitials(Object.values(data)[0].name));
                                Object.values(data)[0].photoUrl=this.getInitials(Object.values(data)[0].name);
                                this.initial=true;
                             }
                              console.log("Data"+JSON.stringify(data));
                              const dateNow = new Date(2024, 1, 25, 13, 30, 30);
                              this.cookieService.set('userObject',JSON.stringify(data),dateNow);
                           
                             localStorage.setItem('userObject', JSON.stringify(data));
                             if(remember)
                             {
                              
                              localStorage.setItem("rem",remember);
                               localStorage.setItem("RememberMe",JSON.stringify(data));
                               
                             }
                              this.router.navigate(["/"]);
                          }
             },
              error => {
                            console.log(error);
              })
}
 getInitials (string) {
  
    var names = string.split(' '),
        initials = names[0].substring(0, 1).toUpperCase();
    
    if (names.length > 1) {
        initials += names[names.length - 1].substring(0, 1).toUpperCase();
    }
    console.log("name first letter is"+initials);
    return initials;
};

}
