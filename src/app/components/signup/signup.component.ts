import { Component, OnInit } from '@angular/core';
import { FormGroup,  FormBuilder, Validators,AbstractControl,FormControl} from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { PostAdsService } from 'src/app/services/post-ads.service';
@Component({
  selector: 'app-signup',
  templateUrl: './signup.component.html',
  styleUrls: ['../../../assets/css/vendor/bootstrap.min.css','../../../assets/css/custom/main-1.css','../../../assets/css/custom/index-1.css','../../../assets/css/custom/modal.css','../../../assets/css/custom/line-icon.css','../../../assets/css/custom/flat-icon.css','../../../assets/css/custom/login.css' ]

})
export class SignupComponent implements OnInit {
  code:any;
  constructor(private formBuilder: FormBuilder, private router: Router,private postAdsServices:PostAdsService) { }
  
   ngOnInit(): void {
   
   
  }
  fieldTextType: boolean;
  toggleFieldTextType() {
  this.fieldTextType = !this.fieldTextType;
}
 loginForm= new FormGroup({
      name:new FormControl('',[Validators.required]),
      email:new FormControl('',[Validators.required,Validators.email]),
      password:new FormControl('',[Validators.required,Validators.minLength(8)]),
      type:new FormControl(''),
      photoUrl:new FormControl('')
    
    });
    loginUser()
    {
    //  document.getElementById("email-text").style="display:none";
      this.loginForm.get("type").setValue("99Discounts");
      this.loginForm.get("photoUrl").setValue("");
      console.log(this.loginForm.value);
              var name= this.loginForm.value.name;
              var email=this.loginForm.value.email;
              var password=this.loginForm.value.password;
              password=window.btoa(password);
              var photoUrl=this.loginForm.value.photoUrl;
              var type=this.loginForm.value.type;
               console.log(this.loginForm.value.name);
        this.postAdsServices.signUp(name,email,photoUrl,type,password)
                  .subscribe(
                    data => {
                      
                      console.log("user data-->"+data);
                        if(Object.values(data)[0].type=="others")
                          {
                          
                            console.log("An account already exists for this email");
      //                      document.getElementById("email-text").style="display:block;color:red !important";
                            //document.getElementById("email-text").innerHTML = "An account already exists for this email";
                            const e: HTMLElement = document.getElementById("email-text");
                            e.style.color = 'red';  
                            e.style.display="block";
                            e.innerHTML="An account already exists for this email" ;
                            //this.router.navigate(["/invalid"]);
                          }else{
                              console.log("success"+Object.values(data)[0].code);
                              //alert("successfully Registered in 99Discount..Please Login to Continue");
                             
                            //this.router.navigate(["/login"]);
                            const e: HTMLElement = document.getElementById("input-code");
                           
                            e.style.display="block";
                            const login: HTMLElement = document.getElementById("login-form");
                           
                            login.style.display="none";
                            this.code=Object.values(data)[0].code;
                            
                            
                          }
                      },
                      error => {
                            console.log(error);
                      })
     
    }
    get name()
    {
      return this.loginForm.get("name");
    }
      get email()
    {
      return this.loginForm.get("email");
    }
      get password()
    {
      return this.loginForm.get("password");
    }
      get type()
    {
      return this.loginForm.get("type").setValue("99Discounts");
    }
    onCodeChanged(code: string) {
      console.log("oncode changed"+code)
   }
    onCodeCompleted(code: string) {
      console.log("complete code"+code);
      if(code==this.code)
      {
       
        this.loginForm.get("type").setValue("99Discounts");
        this.loginForm.get("photoUrl").setValue("");
        var name= this.loginForm.value.name;
        var email=this.loginForm.value.email;
        var password=this.loginForm.value.password;
        password=window.btoa(password);
        var photoUrl=this.loginForm.value.photoUrl;
        var type=this.loginForm.value.type;
        var status="verified";
         console.log(this.loginForm.value.name);
         this.postAdsServices.register(name,email,password,photoUrl,type,status)
                .subscribe(
                   data => {
                     this.postAdsServices.sendEmail(name,email).subscribe(data2=>{console.log("email sended suucessfully")})
                    alert("successfully Registered in 99Discount..Please Login to Continue");
                             
                      this.router.navigate(["/login"]);
                  })

       
      }
      else{
       alert("Incorrect Verification Code");
  
      }
     
    }
}

