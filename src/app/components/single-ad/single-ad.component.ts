import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { PostAdsService } from 'src/app/services/post-ads.service';
import {NgxGalleryOptions} from '@kolkov/ngx-gallery';
import {NgxGalleryImage} from '@kolkov/ngx-gallery';
import {NgxGalleryAnimation} from '@kolkov/ngx-gallery';
import {SocialAuthService} from "angularx-social-login";
 
 

@Component({
  selector: 'app-single-ad',
  templateUrl: './single-ad.component.html',
  styleUrls: ['../../../assets/css/vendor/slick.min.css','../../../assets/css/vendor/bootstrap.min.css','../../../assets/css/custom/main-1.css','../../../assets/css/custom/index-1.css','../../../assets/css/custom/modal.css','../../../assets/css/custom/ad-details.css','../../../assets/css/custom/slider.css']

})
export class SingleAdComponent implements OnInit {

  loggedIn=false;
  adsData:any;
  couponText:any;
  price:any;
  avgRating:any;
  totalRating:any;
  totalUsersRated:any;
  userId:any;
  phoneNumber:any;
  galleryOptions: NgxGalleryOptions[];
  galleryImages: NgxGalleryImage[];
  seller:any;
  loginUserId:any;
  //text = "Lorem ipsum dolor sit amet, consectetur adipiscing elit.";
  constructor( private router: Router, private postAdsServices:PostAdsService,
    private route: ActivatedRoute,private authService: SocialAuthService) { 

     
    }
   
     

     

  ngOnInit() {
    this.route.params.subscribe(params => {
      console.log("params-->",params);
      var id = params['id'];
     
     
    console.log('Url Id: ',id);
    this.couponText = "https://99discounts.in/single-ad/"+id;
      this.getProduct(id);
      this.updateAdsViews(id);

      this.galleryOptions = [
        {
          width: '700px',
          height: '450px',
          thumbnailsColumns: 4,
          arrowPrevIcon: 'fa fa-arrow-circle-left',
          arrowNextIcon: 'fa fa-arrow-circle-right',
          imageAnimation: NgxGalleryAnimation.Slide
        },
        // max-width 800
        {
          breakpoint: 800,
          width: '100%',
          height: '600px',
          imagePercent: 80,
          thumbnailsPercent: 20,
          thumbnailsMargin: 20,
          thumbnailMargin: 20
        },
        // max-width 400
        {
          breakpoint: 400,
          preview: false
        }
      ];
  
      
      this.authService.authState.subscribe(user => {
        console.log("user-->"+user);
        });
    
      if (localStorage.getItem('userObject') !== null) {
        console.log(`userObject exists`);
         var myObjStr=JSON.parse(localStorage.getItem('userObject'));
          console.log(myObjStr);
          
          this.loginUserId=myObjStr[0]._id;
        if(myObjStr==null)
        {
          console.log(`is null`);
          this.loggedIn = false;
        }
        else{
       
         this.loggedIn = true;
        } 
    
    } else {
        console.log(`userObject not found`);
        this.loggedIn = false;
    } 
  });

  

  
  }

  onRatingSet(rating: number): void {
    console.warn(`User set rating to ${rating}`);
    console.log("rating-->",rating);
    //this.avgRating=2;
    var usrObj=JSON.parse(localStorage.getItem('userObject'));
    console.log(usrObj);

    this.totalRating=usrObj[0].avgRating;
    this.totalUsersRated=usrObj[0].noOfUserRated;
    this.avgRating= ((this.totalRating*this.totalUsersRated)+rating)/(this.totalUsersRated+1);
    this.userId=usrObj[0]._id;
    this.avgRating=this.avgRating.toFixed(2);
    
     
    this.postAdsServices.saveUserAvgRating(this.avgRating,this.userId)
    .subscribe(
      data => {
         console.log("rating --->",data);
      },
      error => {
        console.log(error);
      });
  }
   
  

  
  goToLink(url: string){
    window.open(url, "_blank");
}
getProduct(id)
{
  this.postAdsServices.getProductAd(id)
  .subscribe(
    data => {
      this.adsData = data;
      this.price=data[0].price;
      this.phoneNumber=data[0].mobile_no;
      console.log(this.phoneNumber);
      //this.categoryMetaData="";
      console.log(data);
      localStorage.setItem(data[0]._id,data[0].title);
      this.galleryImages = [
        {
          small: data[0].image_url1,
          medium: data[0].image_url1,
          big: data[0].image_url1
        },
        {
          small: data[0].image_url2,
          medium: data[0].image_url2,
          big: data[0].image_url2
        },
        {
          small: data[0].image_url3,
          medium: data[0].image_url3,
          big: data[0].image_url3
        },{
          small:data[0].image_url1 ,
          medium:data[0].image_url1,
          big: data[0].image_url1
        },     
        {
          small: data[0].image_url2,
          medium: data[0].image_url2,
          big: data[0].image_url2
        },{
          small: data[0].image_url3,
          medium: data[0].image_url3,
          big:data[0].image_url3
        },      
      ];

      this.postAdsServices.getSellerInfo(data[0].createdBy)
      .subscribe(
        data => {
            console.log("datattata",data);
            this.seller=data[0];
            this.avgRating=this.seller.avgRating;
            localStorage.setItem(this.seller._id,this.seller.name);
        },
        error =>{

          console.log(error);
        }
      );
    },
    error => {
      console.log(error);
    });
}
 

updateAdsViews(id)
{
  this.postAdsServices.updateAdsViews(id)
  .subscribe(
    data => {
      console.log("update data",data);

    },
    error =>{

      console.log(error);
    });

}
 
}
