import { Component, OnInit } from '@angular/core';
import { UploadFilesService } from 'src/app/services/upload-files.service';
import { HttpEventType, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';
@Component({
  selector: 'app-upload-images',
  templateUrl: './upload-images.component.html',
  styleUrls: ['./upload-images.component.css']
})
export class UploadImagesComponent {

  
  selectedFiles: FileList;
  progressInfos = [];
  message2 = false;
  message = '';
  previews: string[] = [];
  fileInfos: Observable<any>;

  constructor(private uploadService: UploadFilesService) { }
  ngOnInit() {
    this.fileInfos = this.uploadService.getFiles();
  }
  selectFiles(event) {
    this.progressInfos = [];
  
    const files = event.target.files;
    let isImage = true;
    this.previews = []; 
  
    for (let i = 0; i < files.length; i++) {
      if (files.item(i).type.match('image.*')) {
        continue;
      } else {
        isImage = false;
        alert('invalid format!');
        break;
      }
    }
  
    if (isImage) {
      this.selectedFiles = event.target.files;
    } else {
      this.selectedFiles = undefined;
      event.srcElement.percentage = null;
    }
  }
  selectFiles2(event: any): void {
  
    this.progressInfos = [];
    this.selectedFiles = event.target.files;
  
    this.previews = [];
    if (this.selectedFiles && this.selectedFiles[0]) {
      const numberOfFiles = this.selectedFiles.length;
      for (let i = 0; i < numberOfFiles; i++) {
        const reader = new FileReader();
  
        reader.onload = (e: any) => {
          console.log(e.target.result);
          this.previews.push(e.target.result);
        };
  
        reader.readAsDataURL(this.selectedFiles[i]);
      }
    }
  }
      
  upload(idx, file) {
    console.log("upload file---")
    this.progressInfos[idx] = { value: 0, fileName: file.name };
  
    this.uploadService.upload2(file,idx).subscribe(
      event => {
     if (event.type === HttpEventType.UploadProgress) {
          this.progressInfos[idx].percentage = Math.round(100 * event.loaded / event.total);
          console.log("inside of upload11");
          this.message2=true;
        } else if (event instanceof HttpResponse) {
          this.fileInfos = this.uploadService.getFiles();
          console.log("inside of upload22")
         
        }
        console.log("inside of upload33")
      },
      err => {
        this.progressInfos[idx].percentage = 0;
        this.message = 'Could not upload the file:' + file.name;
      });
  }
  uploadFiles() {
    this.message = '';
  
    for (let i = 0; i < this.selectedFiles.length; i++) {
      this.upload(i, this.selectedFiles[i]);
    }
  }

}
