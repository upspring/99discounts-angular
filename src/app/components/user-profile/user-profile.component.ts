import { Component, OnInit } from '@angular/core';
import { FormGroup,  FormBuilder, Validators,AbstractControl,FormControl} from '@angular/forms';
import { PostAdsService } from 'src/app/services/post-ads.service';
import { WindowService } from 'src/app/services/window.service';
import * as firebase from 'firebase';
import { ActivatedRoute, Router } from '@angular/router';
@Component({
  selector: 'app-user-profile',
  templateUrl: './user-profile.component.html',
  styleUrls: ['../../../assets/css/vendor/slick.min.css','../../../assets/css/vendor/bootstrap.min.css','../../../assets/css/custom/main-1.css','../../../assets/css/custom/index-1.css','../../../assets/css/custom/modal.css','../../../assets/css/custom/line-icon.css','../../../assets/css/custom/flat-icon.css']
})
export class UserProfileComponent implements OnInit {
  socialUser:any;
  email:any;name:any;photoUrl:any;
  initial=false;
  isVerified=false;
  windowRef: any;
  verificationCode: string;
  num:any;
  age:any;
  gender:any;
  constructor(private formBuilder: FormBuilder,private postAdsServices:PostAdsService,private win: WindowService,private router: Router) { 
    const firebaseConfig = {
      apiKey: "AIzaSyA3OT3a5K9IT_gAlC7ybfs1zHCfc8qtC8c",
      authDomain: "discounts-bdc9b.firebaseapp.com",
      projectId: "discounts-bdc9b",
      storageBucket: "discounts-bdc9b.appspot.com",
      messagingSenderId: "167744934208",
      appId: "1:167744934208:web:61a4fff99ab995d0936aa5",
      measurementId: "G-BNKXDSKHPP"
    };
    if (!firebase.apps.length) {
      firebase.initializeApp(firebaseConfig);
    }
  }

  ngOnInit(): void {
    if (localStorage.getItem("userObject") === null) {
      console.log("empty")
    } 
    else{
       console.log("not empty");
       this.socialUser=JSON.parse(localStorage.getItem('userObject'));
        //console.log(this.socialUser.name);
        this.email=this.socialUser[0].email;
        this.name=this.socialUser[0].name;
        
        if(this.socialUser[0].type=="99Discounts")
        {
          console.log("its 99"+this.socialUser[0].photoUrl)
          this.initial=true;
        }
        if(this.socialUser[0].photoUrl=="")
        {
          this.photoUrl=this.socialUser[0].name.charAt(0);
        }
        else{this.photoUrl=this.socialUser[0].photoUrl;}
        
        this.loginForm.get("email").setValue(this.socialUser[0].email);
        this.loginForm.get("name").setValue(this.socialUser[0].name);
        this.loginForm.get("age").setValue(this.socialUser[0].age);
        this.loginForm.get("gender").setValue(this.socialUser[0].gender);
        if(this.socialUser[0].phone_status=="verified")
        {
          this.isVerified=true;
           this.loginForm.get("phone_number").setValue(this.socialUser[0].phone_number);
        
        }
       
    }
    this.windowRef = this.win.windowRef;
      
    // this.windowRef.recaptchaVerifier = new firebase.auth.RecaptchaVerifier('recaptcha-container');
   this.windowRef.recaptchaVerifier = new firebase.auth.RecaptchaVerifier(
     "recaptcha-container",
     {
       size: "invisible",
       callback: function(response) {
         //submitPhoneNumberNumber();
       }
     }
   );
     this.windowRef.recaptchaVerifier.render();
     console.log("recap"+this.windowRef.recaptchaVerifier)
  }
  loginForm= new FormGroup({
    name:new FormControl('',[Validators.required]),
    email:new FormControl('',[Validators.required,Validators.email]),
    phone_number:new FormControl(''),
    gender:new FormControl(''),
    age:new FormControl('')
  
  });
  move(e:any, p:any, c:any, n:any){
    var length = c.value.length;
    var maxlength = c.getAttribute("maxlength");
    if(length == maxlength){
      if(n != ""){
        n.focus();
      }
    }
    if(e.key === "Backspace"){
      console.log("backspace pressed");
        if(p != ""){
          p.focus();
        }
    }
  }
  otpClose(){
    document.getElementById('otp-box').style.display="none";
  }
  editUser()
  {
    console.log(this.loginForm.value.email);
    console.log(this.loginForm.value.phone_number);
    var email=this.loginForm.value.email;
    var name=this.loginForm.value.name;
    var phone_number=this.loginForm.value.phone_number.toString();
    var gender=this.loginForm.value.gender;
    var age=this.loginForm.value.age;
    console.log(this.socialUser[0].phone_status);
    console.log(this.socialUser[0].age)
    console.log(this.socialUser[0].gender)
    console.log(this.socialUser[0].phone_number)
    console.log("age value"+this.loginForm.value.age)
   
    console.log("phone status--->"+this.socialUser[0].phone_status)

      if(gender=="" && age==""&&phone_number=="")
        {
          console.log("all data are empty");
        }
       else{
         console.log("else....");
         if(age==null){age=this.socialUser[0].age}
         if(phone_number==null){age=this.socialUser[0].phone_number}
         if(age!=null)
         {
         if(age.toString().length>0){
           console.log("age present ahhh")
           age=this.loginForm.value.age}else{
           age=this.socialUser[0].age;
          }
         }
         if(gender.length>0){gender=this.loginForm.value.gender}else{
          gender=this.socialUser[0].gender;
         }
         if(this.socialUser[0].phone_number!="")
          {
            console.log("phone not empty")
           
              if(phone_number==this.socialUser[0].phone_number)
              {
                console.log("number same");
                phone_number=this.socialUser[0].phone_number;
              }else
              {  const e: HTMLElement = document.getElementById("otp-box");
              e.style.display="block";
              // e.style.marginTop="-100px";
              
              this.sendLoginCode();

              }
          }
          else{
            console.log("phone empty")
            if(this.socialUser[0].phone_status=="unverified")
            {
              if(phone_number.length>0)
             {
              const e: HTMLElement = document.getElementById("otp-box");
              e.style.display="block";
              // e.style.marginTop="-100px";
              
              this.sendLoginCode();
             }
            }
          }
           console.log("age"+age);
        console.log("gender"+gender);
        console.log("phone"+phone_number)
        console.log("phone status"+this.socialUser[0].phone_status)
        var status=this.socialUser[0].phone_status;
        this.age=age;
        this.gender=gender;
        this.num=phone_number;
      
          this.postAdsServices.editUser(email,name,this.socialUser[0].phone_number,gender,age,status)
          .subscribe(
            response => {
              console.log(response);
              
              console.log("sucessfully inserted");
              Object.values(response)[0].age=age;
              Object.values(response)[0].gender=gender;
              localStorage.setItem('userObject', JSON.stringify(response));
             const e: HTMLElement = document.getElementById("alert");
                            e.style.color = 'red';  
                            e.style.display="block";
                            e.style.marginLeft="20px"
              //this.router.navigate(["/"]);
            },
            error => {
              console.log(error);
            });
      
      }
        
    
     
      
      
  }
  sendLoginCode() {
    const e: HTMLElement = document.getElementById("alert");
 
    e.style.display="none";
    console.log("send otp")
    console.log(this.loginForm.value.phone_number.toString());
   
    const number=`+91${this.loginForm.value.phone_number.toString()}`;
          const appVerifier = this.windowRef.recaptchaVerifier;
          
           console.log("num"+number);
        
           
           firebase.auth().signInWithPhoneNumber(number,appVerifier).then(result => {
             console.log("result"+result);
            this.windowRef.confirmationResult = result;
           })
           .catch( error => console.log(error) );
   }
   onCodeChanged(code: string) 
   {
     console.log(code);
     this.verificationCode=code;
   } 
   onCodeCompleted(code: string) 
   {
     console.log(code);
     this.verificationCode=code;
   }
         verifyLoginCode() {
          console.log("verification code id"+this.verificationCode);
          this.windowRef.confirmationResult.confirm(this.verificationCode).then( result => {
           // this.user = result.user;
           var email=this.loginForm.value.email;
           var mobile_no=this.num;
           var name=this.loginForm.value.name;
         
            var gender=this.gender;
            var age=this.age;
            var status="verified";
            console.log("age"+age);
            console.log("gender"+gender);
            console.log("phone"+mobile_no)
           this.postAdsServices.editUser(email,name,mobile_no,gender,age,status)
            .subscribe(response2 => {
              console.log("statis"+ Object.values(response2)[0].phone_status)
              console.log("statis"+ JSON.stringify(response2))
              Object.values(response2)[0].phone_status="verified";
              Object.values(response2)[0].phone_number=mobile_no;
              console.log("statis"+ JSON.stringify(response2))
              localStorage.setItem('userObject', JSON.stringify(response2));
              window.location.reload();
            });
         
          })
          .catch( error => alert( "Incorrect code entered?"));
        }
}
