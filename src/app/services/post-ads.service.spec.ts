import { TestBed } from '@angular/core/testing';

import { PostAdsService } from './post-ads.service';

describe('PostAdsService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: PostAdsService = TestBed.get(PostAdsService);
    expect(service).toBeTruthy();
  });
});
