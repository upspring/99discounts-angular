import { Injectable } from '@angular/core';
import { HttpClient ,HttpHeaders } from '@angular/common/http';

const baseUrl = 'https://99discounts.in/api';
//const baseUrl = 'http://localhost:8080/api';
@Injectable({
  providedIn: 'root'
})


export class PostAdsService {

  constructor(private http: HttpClient) { }
  saveAd(data) {
    return this.http.post(`${baseUrl}/saveAds`,data);
  }
  getMetadata(id)
  {
    return this.http.get(`${baseUrl}/getMetadata/${id}`);
  }
  getCategory(id)
  {
    return this.http.get(`${baseUrl}/getCategory/${id}`);
  }
  getAdsData()
  {
    return this.http.get(`${baseUrl}/getAdsData`);
  }
  getAllCategory()
  {
    return this.http.get(`${baseUrl}/getAllCategory`);
  }
  getMainCategory()
  {
    return this.http.get(`${baseUrl}/getMainCategory`);
  }
  getAdsForCategory(category)
  {
    return this.http.get(`${baseUrl}/getAdsForCategory/${category}`);
  }
  getAllCategory2()
  {
    return this.http.get(`${baseUrl}/getAllCategory2`);
  }
  getProductAd(id)
  {
    return this.http.get(`${baseUrl}/getProductAd/${id}`);
  }
  searchTerm(word)
  {
    return this.http.get(`${baseUrl}/searchTerm/${word}`);
     
}
getBrandName()
{
 
  return this.http.get(`${baseUrl}/getBrandName`);
}
getSearchData(search,location,category)
{
  return this.http.get(`${baseUrl}/searchData?search=${search}&location=${location}&category=${category}`);
}
saveUser(name,email,photoUrl,provider)
{
  return this.http.get(`${baseUrl}/saveUser?name=${name}&email=${email}&photoUrl=${photoUrl}&type=${provider}`)
}
oneTapLogin(res)
{
   return this.http.post(`${baseUrl}/oneTapLogin`,res);
}
signUp(name,email,photoUrl,provider,password)
{
  return this.http.get(`${baseUrl}/signUp?name=${name}&email=${email}&photoUrl=${photoUrl}&type=${provider}&password=${password}`)

}
register(name,email,password,photoUrl,provider,status)
{
  return this.http.get(`${baseUrl}/register?name=${name}&email=${email}&photoUrl=${photoUrl}&type=${provider}&password=${password}&status=${status}`)

}

login(email,password,provider)
{
  return this.http.get(`${baseUrl}/login?email=${email}&password=${password}&type=${provider}`)

}


saveUserAvgRating(rating,userId)
{
   
  return this.http.get(`${baseUrl}/saveUserAvgRating?avgRating=${rating}&userId=${userId}`)
}

getSellerInfo(emailId)
{
  return this.http.get(`${baseUrl}/getSellerInfo?emailId=${emailId}`)
}

getSellerDetails(id)
{
  return this.http.get(`${baseUrl}/getSellerDetails?id=${id}`)
}

checkEmail(email)
{
   return this.http.get(`${baseUrl}/checkEmail?email=${email}`)
}
sendEmail(name,email)
{
  return this.http.get(`${baseUrl}/sendWelcome?email=${email}&name=${name}`)
}
sendVerificationEmail(email)
{
  return this.http.get(`${baseUrl}/sendVerificationEmail?email=${email}`)
}
updatePassword(email,password)
{
  return this.http.get(`${baseUrl}/updatePassword?email=${email}&password=${password}`)
}
getMyAds(email)
{
  return this.http.get(`${baseUrl}/getMyAds?email=${email}`)
}
deleteMyAds(id)
{
  return this.http.get(`${baseUrl}/deleteMyAds?id=${id}`)
}
getAlternativeRecomment(url)
{
  return this.http.get(`${baseUrl}/checkUrlExists?url=${url}`)
}


getViewOffers(url)
{
  return this.http.get(`${baseUrl}/getViewOffers?url=${url}`)
}
updateAdsViews(id)
{
  return this.http.get(`${baseUrl}/updateAdsViews?id=${id}`)
}
updateUser(email,number)
{
  return this.http.get(`${baseUrl}/updateUser?email=${email}&number=${number}`)
}
editUser(email,name,number,gender,age,status)
{
  console.log(status);
  return this.http.get(`${baseUrl}/editUser?email=${email}&name=${name}&number=${number}&gender=${gender}&age=${age}&status=${status}`)
}

sendOfferEmail(price,content,email,title,userName,userPrice)
{
  return this.http.get(`${baseUrl}/sendOfferEmail?price=${price}&content=${content}&email=${email}&title=${title}&name=${userName}&userPrice=${userPrice}`)
}


}




