import { Injectable } from '@angular/core';
import { HttpClient, HttpRequest, HttpEvent } from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class UploadFilesService {

  private baseUrl = 'https://99discounts.in/api/tutorials';

  constructor(private http: HttpClient) { }

  upload(file: File,idx): Observable<HttpEvent<any>> {
    console.log("services....");
    const formData: FormData = new FormData();

    formData.append('file', file);
    formData.append("index",idx);

    const req = new HttpRequest('POST', `https://99discounts.in/api/upload`, formData, {
      reportProgress: true,
      responseType: 'json'
    });

    return this.http.request(req);
  }
  upload2(file: File,idx): Observable<HttpEvent<any>> {
    console.log("services....");
    const formData: FormData = new FormData();

    formData.append('file', file);
    formData.append("index",idx);

    const req = new HttpRequest('POST', `https://99discounts.in/api/upload2`, formData, {
      reportProgress: true,
      responseType: 'json'
    });

    return this.http.request(req);
  }


  getFiles(): Observable<any> {
    return this.http.get(`${this.baseUrl}/files`);
  }
}